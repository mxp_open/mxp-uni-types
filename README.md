# 介绍

基于[@uni-helper/uni-app-types](https://github.com/uni-helper/uni-app-types)实现用于 uniapp 的原生组件类型

## 安装

```js

npm i mxp-uni-types
// 或
pnpm i mxp-uni-types
// 或
yarn add mxp-uni-types

```

## 使用

配置 `tsconfig.json`，确保 c`ompilerOptions.types` 中含有 `@dcloudio/types` 和 `mxp-uni-types`

```json
{
  "compilerOptions": {
    "types": ["@dcloudio/types", "mxp-uni-types"]
  },
  "include": ["src/**/*.vue"]
}
```

## 标注类型

推荐使用 `mxp-uni-types` 导出的类型为变量标注类型。

```vue
<script setup lang="ts">
  import { ref } from 'vue';
  import type { ScrollViewOnScroll } from 'mxp-uni-types';

  const onScroll: ScrollViewOnScroll = (event) => {
    ...
  };
</script>

<template>
  <scroll-view @scroll="onScroll"></scroll-view>
</template>
```

也可以直接使用命名空间来为变量标注类型。

```vue
<script setup lang="ts">
  import { ref } from 'vue';

  const onScroll: GUni.ScrollViewOnScroll = (event) => {
    ...
  };
</script>

<template>
  <scroll-view @scroll="onScroll"></scroll-view>
</template>
```

如果你需要传入事件之外的参数，可以参考以下例子。参数顺序参考了 Vue 文档的示例（见 [在内联事件处理器中访问事件参数](https://cn.vuejs.org/guide/essentials/event-handling.html#accessing-event-argument-in-inline-handlers)）。

```vue
<script setup lang="ts">
  import { ref } from 'vue';
  import type { ScrollViewOnScrollEvent } from 'mxp-uni-types';

  const onScroll = (text: string, event: ScrollViewOnScrollEvent) => {
    ...
  };
</script>

<template>
  <scroll-view @scroll="onScroll('ScrollViewA', $event)"></scroll-view>
  <scroll-view @scroll="onScroll('ScrollViewB', $event)"></scroll-view>
</template>
```

## 致谢

基于[@uni-helper/uni-app-types](https://github.com/uni-helper/uni-app-types)实现
