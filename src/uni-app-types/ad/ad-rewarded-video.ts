/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../utils/component';
import type { BaseEvent, CustomEvent } from '../events';

/** 服务器回调透传数据 */
interface _AdRewardedVideoUrlCallback {
  userId: string;
  extra: string;
}

type _AdRewardedVideoOnLoadEvent = BaseEvent;

/** 广告加载成功的回调 */
interface _AdRewardedVideoOnLoad {
  (event: _AdRewardedVideoOnLoadEvent): void;
}

interface _AdRewardedVideoOnErrorDetail {
  /** 错误码 */
  errCode: number;
  /** 错误信息 */
  errMsg: string;
}

type _AdRewardedVideoOnErrorEvent = CustomEvent<_AdRewardedVideoOnErrorDetail>;

/** 广告加载失败的回调 */
interface _AdRewardedVideoOnError {
  (event: _AdRewardedVideoOnErrorEvent): void;
}

type _AdRewardedVideoOnCloseEvent = BaseEvent;

/** 广告关闭的回调 */
interface _AdRewardedVideoOnClose {
  (event: _AdRewardedVideoOnCloseEvent): void;
}

/** 激励视频广告属性 */
type _AdRewardedVideoProps = Partial<{
  /** APP 广告位 id */
  adpid: (number | string)[] | number | string;
  /**
   * 是否在页面就绪后加载广告数据
   *
   * 默认为 true
   */
  preload: boolean;
  /**
   * 是否自动加载下一条广告数据
   *
   * 默认为 false
   */
  loadnext: boolean;
  /** 服务器回调透传数据 */
  urlCallback: _AdRewardedVideoUrlCallback;
  /** 广告加载成功的回调 */
  onLoad: _AdRewardedVideoOnLoad;
  /** 广告加载失败的回调 */
  onError: _AdRewardedVideoOnError;
  /** 广告关闭的回调 */
  onClose: _AdRewardedVideoOnClose;
}>;

/** 激励视频广告 */
type _AdRewardedVideo = Component<_AdRewardedVideoProps>;

/** 激励视频广告实例 */
type _AdRewardedVideoInstance = InstanceType<_AdRewardedVideo>;

export type {
  _AdRewardedVideoUrlCallback as AdRewardedVideoUrlCallback,
  _AdRewardedVideoOnLoadEvent as AdRewardedVideoOnLoadEvent,
  _AdRewardedVideoOnLoad as AdRewardedVideoOnLoad,
  _AdRewardedVideoOnErrorDetail as AdRewardedVideoOnErrorDetail,
  _AdRewardedVideoOnErrorEvent as AdRewardedVideoOnErrorEvent,
  _AdRewardedVideoOnError as AdRewardedVideoOnError,
  _AdRewardedVideoOnCloseEvent as AdRewardedVideoOnCloseEvent,
  _AdRewardedVideoOnClose as AdRewardedVideoOnClose,
  _AdRewardedVideoProps as AdRewardedVideoProps,
  _AdRewardedVideo as AdRewardedVideo,
  _AdRewardedVideoInstance as AdRewardedVideoInstance,
};

declare global {
  namespace GUni {
    /** 服务器回调透传数据 */
    export type AdRewardedVideoUrlCallback = _AdRewardedVideoUrlCallback;
    export type AdRewardedVideoOnLoadEvent = _AdRewardedVideoOnLoadEvent;
    /** 广告加载成功的回调 */
    export type AdRewardedVideoOnLoad = _AdRewardedVideoOnLoad;
    export type AdRewardedVideoOnErrorDetail = _AdRewardedVideoOnErrorDetail;
    export type AdRewardedVideoOnErrorEvent = _AdRewardedVideoOnErrorEvent;
    /** 广告加载失败的回调 */
    export type AdRewardedVideoOnError = _AdRewardedVideoOnError;
    export type AdRewardedVideoOnCloseEvent = _AdRewardedVideoOnCloseEvent;
    /** 广告关闭的回调 */
    export type AdRewardedVideoOnClose = _AdRewardedVideoOnClose;
    /** 激励视频广告属性 */
    export type AdRewardedVideoProps = _AdRewardedVideoProps;
    /** 激励视频广告 */
    export type AdRewardedVideo = _AdRewardedVideo;
    /** 激励视频广告实例 */
    export type AdRewardedVideoInstance = _AdRewardedVideoInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /** 激励视频广告 */
    AdRewardedVideo: _AdRewardedVideo;
  }
}
