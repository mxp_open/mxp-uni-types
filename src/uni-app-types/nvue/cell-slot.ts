/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';

/** nvue专用 CellSlot 容器属性 */
type _CellSlotProps = Partial<{
  /**
   * 声明了当前模板的类型，只有和数据中的类型与当前类型匹配时才会渲染，语义和编程语言里的 case 一致。所有模板中最多只会匹配到一项，按照模板的顺序从上到下匹配，一旦匹配成功就不在继续匹配下一个。
   */
  case: boolean | number | string;

  /**
   * 表示当前模板为默认模板类型，不需要指定值。如果数据项没有匹配到任何 case 类型，则渲染带有 default 模板。如果存在多个 default，则只会使用第一个默认模板。
   */
  default: boolean | number | string;
  /**
   *可选属性，用于指定列表数据中可以作为唯一标识的键值，可以优化渲染性能。
   */
  key: number | string;
}>;

/**
 * app端nvue专用组件。 <cell-slot> 只能在<recycle-list> 中 作为其直接子节点，使用其他节点无效
 *
 * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
 */
type _CellSlot = Component<_CellSlotProps>;

/** nvue专用 CellSlot 容器实例 */
type _CellSlotInstance = InstanceType<_CellSlot>;

export type { _CellSlotProps as CellSlotProps, _CellSlot as CellSlot, _CellSlotInstance as CellSlotInstance };

declare global {
  namespace GUni {
    /** nvue专用 CellSlot 容器属性 */
    export type CellSlotProps = _CellSlotProps;
    /**
     * app端nvue专用组件。 <cell-slot> 只能在<recycle-list> 中 作为其直接子节点，使用其他节点无效
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
     */
    export type CellSlot = _CellSlot;
    /** 视图容器实例 */
    export type CellSlotInstance = _CellSlotInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。 <cell-slot> 只能在<recycle-list> 中 作为其直接子节点，使用其他节点无效
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
     */
    CellSlot: _CellSlot;
  }
}
