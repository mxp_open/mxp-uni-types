/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';

/** nvue专用 Cell 容器属性 */
type _CellProps = Partial<{
  /**
   * 它控制插入单元格后是否保持最后一个滑动位置。
   */
  keepScrollPosition: boolean;

  /**
   * cell 的插入动画。当前只支持 none 和 default
   */
  insertAnimation: 'default' | 'none';
  /**
   * cell 的删除动画。当前只支持 none 和 default
   */
  deleteAnimation: 'default' | 'none';
  /**
   * 默认值 true。这个属性控制这个 Cell 的 view 和子 views 是否在列表滚动时进行回收，在 iOS 上通常必须指定为 true （因为默认为 true，所以一般不需要写这个属性），如果设置为 false，列表滚动时，页面会占用非常高的内存。Android上默认是true，设置为false可以防止Image和Text上数据重新绑定。
   */
  recycle: boolean;
  /**
   * 默认值 false。定义开始渲染的位置，需搭配list的属性render-reverse共同使用，单独配置不起作用。HBuilderX3.6.9+支持
   */
  renderPeversePosition: boolean;
}>;

/**
 * app端nvue专用组件。它的重要价值在于告诉原生引擎，哪些部分是可重用的。 <cell> Cell 必须以一级子组件的形式存在于 list recycler waterfall 中。
 *
 * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/cell.html}
 */
type _Cell = Component<_CellProps>;

/** nvue专用 Cell 容器实例 */
type _CellInstance = InstanceType<_Cell>;

export type { _CellProps as CellProps, _Cell as Cell, _CellInstance as CellInstance };

declare global {
  namespace GUni {
    /** nvue专用 Cell 容器属性 */
    export type CellProps = _CellProps;
    /**
     * app端nvue专用组件。它的重要价值在于告诉原生引擎，哪些部分是可重用的。 <cell> Cell 必须以一级子组件的形式存在于 list recycler waterfall 中。
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/cell.html}
     */
    export type Cell = _Cell;
    /** 视图容器实例 */
    export type CellInstance = _CellInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。它的重要价值在于告诉原生引擎，哪些部分是可重用的。 <cell> Cell 必须以一级子组件的形式存在于 list recycler waterfall 中。
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/cell.html}
     */
    Cell: _Cell;
  }
}
