/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';
import type { CustomEvent } from '../events';

/** 当 <scroll-view>、<list>、<waterfall> 被下拉完成时触发。 */
interface _RefreshOnRefresh {
  (event: any): void;
}

interface _RefreshOnPullingdownDetail {
  /** 前后两次回调滑动距离的差值 */
  dy: number;
  /** 下拉的距离 */
  pullingDistance: number;
  /** refresh 组件高度 */
  viewHeight: number;
  /** 常数字符串 如“pullingdown” */
  type: string;
}

type _RefreshOnPullingdownEvent = CustomEvent<_RefreshOnPullingdownDetail>;
/** 当 <scroll-view>、<list>、<waterfall> 被下拉时触发。  */
interface _RefreshOnPullingdown {
  (event: _RefreshOnPullingdownEvent): void;
}

/** nvue专用 Refresh 容器属性 */
type _RefreshProps = Partial<{
  /**
   * 控制 <refresh> 组件显示、隐藏。display 的设置必须成对出现，即设置 display="show", 必须有对应的 display="hide"。可选值为 show / hide，默认值为 show。
   */
  display: any;
  /**
   * <scroll-view>、<list>、<waterfall> 被下拉完成时触发。
   */
  onRefresh: _RefreshOnRefresh;

  onPullingdown: _RefreshOnPullingdown;
}>;

/**
 * app端nvue专用组件。<refresh> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
 *
 * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
 */
type _Refresh = Component<_RefreshProps>;

/** nvue专用 Refresh 容器实例 */
type _RefreshInstance = InstanceType<_Refresh>;

export type { _RefreshProps as RefreshProps, _Refresh as Refresh, _RefreshInstance as RefreshInstance };

declare global {
  namespace GUni {
    /** nvue专用 Refresh 容器属性 */
    export type RefreshProps = _RefreshProps;
    /**
     * app端nvue专用组件。<refresh> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
     *
     * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
     */
    export type Refresh = _Refresh;
    /** 视图容器实例 */
    export type RefreshInstance = _RefreshInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。<refresh> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
     *
     * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
     */
    Refresh: _Refresh;
  }
}
