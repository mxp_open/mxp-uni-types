/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';

/** nvue专用 Waterfall 容器属性 */
type _WaterfallProps = Partial<{
  /**
   * 可选值为 true/ false，默认值为 true。控制是否出现滚动条。
   */
  showScrollbar: boolean;
  /**
   * 描述瀑布流的列数
   * 当值为‘auto’ 意味着列数是被其他属性所决定的(比如 column-width)
   */
  columnCount: number | 'auto';
  /**
   * 描述瀑布流每一列的列宽
   * 当值为‘auto’ 意味着列宽是被其他属性所决定的(比如 column-count)
   */
  columnWidth: number | 'auto';
  /**
   * 列与列的间隙. 如果指定了 normal ，则对应 32.
   */
  columnGap: number;
  /**
   * 左边cell和列表的间隙. 如果未指定 ，则对应 0
   */
  leftGap: number;
}>;

/**
 * app端nvue专用组件。<waterfall> 组件是提供瀑布流布局的核心组件。瀑布流，又称瀑布流式布局，是比较流行的一种页面布局，视觉表现为参差不齐的多栏布局。随着页面滚动条向下滚动，这种布局还可以不断加载数据块并附加至当前尾部。
 *
 * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/waterfall.html}
 */
type _Waterfall = Component<_WaterfallProps>;

/** nvue专用 Waterfall 容器实例 */
type _WaterfallInstance = InstanceType<_Waterfall>;

export type { _WaterfallProps as WaterfallProps, _Waterfall as Waterfall, _WaterfallInstance as WaterfallInstance };

declare global {
  namespace GUni {
    /** nvue专用 Waterfall 容器属性 */
    export type WaterfallProps = _WaterfallProps;
    /**
     * app端nvue专用组件。<waterfall> 组件是提供瀑布流布局的核心组件。瀑布流，又称瀑布流式布局，是比较流行的一种页面布局，视觉表现为参差不齐的多栏布局。随着页面滚动条向下滚动，这种布局还可以不断加载数据块并附加至当前尾部。
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/waterfall.html}
     */
    export type Waterfall = _Waterfall;
    /** 视图容器实例 */
    export type WaterfallInstance = _WaterfallInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。<waterfall> 组件是提供瀑布流布局的核心组件。瀑布流，又称瀑布流式布局，是比较流行的一种页面布局，视觉表现为参差不齐的多栏布局。随着页面滚动条向下滚动，这种布局还可以不断加载数据块并附加至当前尾部。
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/waterfall.html}
     */
    Waterfall: _Waterfall;
  }
}
