/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';

/** nvue专用 list 容器属性 */
type _ListProps = Partial<{
  /**
   * 控制是否出现滚动条
   * 默认：true
   */
  showScrollbar: boolean;

  /**
   * 控制是否回弹效果, iOS 不支持动态修改
   * 默认：true
   */
  bounce: boolean;
  /**
   * 触发 loadmore 事件所需要的垂直偏移距离(设备屏幕底部与 list 底部之间的距离)，建议手动设置此值，设置大于0的值即可
   * 默认 0
   */
  loadmoreoffset: number;
  /**
   * 控制 onscroll 事件触发的频率：表示两次onscroll事件之间列表至少滚动了10px。注意，将该值设置为较小的数值会提高滚动事件采样的精度，但同时也会降低页面的性能
   * 默认 10
   */
  offsetAccuracy: number;
  /**
   * 是否按分页模式显示List
   * 默认：false
   */
  pagingEnabled: boolean;
  /**
   * 控制是否出现滚动条
   * 默认：true
   */
  scrollable: boolean;
  /**
   * 是否允许List滚动
   */
  enableBackToTop: boolean;
  /**
   * 定义是否从底部渲染，需搭配cell的属性render-reverse-position共同使用，单独配置会导致渲染异常。HBuilderX3.6.9+支持
   * 默认：false
   */
  renderReverse: boolean;
}>;

/**
 * app端nvue专用组件。在app-nvue下，如果是长列表，使用list组件的性能高于使用view或scroll-view的滚动。原因在于list在不可见部分的渲染资源回收有特殊的优化处理。
 * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/list.html}
 */
type _List = Component<_ListProps>;

/** nvue专用 list 容器实例 */
type _ListInstance = InstanceType<_List>;

export type { _ListProps as ListProps, _List as List, _ListInstance as ListInstance };

declare global {
  namespace GUni {
    /** nvue专用 list 容器属性 */
    export type ListProps = _ListProps;
    /**
     * app端nvue专用组件。在app-nvue下，如果是长列表，使用list组件的性能高于使用view或scroll-view的滚动。原因在于list在不可见部分的渲染资源回收有特殊的优化处理。
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/list.html}
     */
    export type List = _List;
    /** 视图容器实例 */
    export type ListInstance = _ListInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。在app-nvue下，如果是长列表，使用list组件的性能高于使用view或scroll-view的滚动。原因在于list在不可见部分的渲染资源回收有特殊的优化处理。
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/list.html}
     */
    List: _List;
  }
}
