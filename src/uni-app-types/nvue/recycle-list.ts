/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';

/** nvue专用 RecycleList 容器属性 */
type _RecycleListProps = Partial<{
  /**
   * 用法类似 v-for
   */
  for: any;
}>;

/**
 * app端nvue专用组件。 <recycle-list> 是一个新的列表容器，具有回收和复用的能力，可以大幅优化内存占用和渲染性能。它的性能比list组件更高，但写法受限制。它除了会释放不可见区域的渲染资源，在非渲染的数据结构上也有更多优化
 *
 * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
 */
type _RecycleList = Component<_RecycleListProps>;

/** nvue专用 RecycleList 容器实例 */
type _RecycleListInstance = InstanceType<_RecycleList>;

export type { _RecycleListProps as RecycleListProps, _RecycleList as RecycleList, _RecycleListInstance as RecycleListInstance };

declare global {
  namespace GUni {
    /** nvue专用 RecycleList 容器属性 */
    export type RecycleListProps = _RecycleListProps;
    /**
     * app端nvue专用组件。 <recycle-list> 是一个新的列表容器，具有回收和复用的能力，可以大幅优化内存占用和渲染性能。它的性能比list组件更高，但写法受限制。它除了会释放不可见区域的渲染资源，在非渲染的数据结构上也有更多优化
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
     */
    export type RecycleList = _RecycleList;
    /** 视图容器实例 */
    export type RecycleListInstance = _RecycleListInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。 <recycle-list> 是一个新的列表容器，具有回收和复用的能力，可以大幅优化内存占用和渲染性能。它的性能比list组件更高，但写法受限制。它除了会释放不可见区域的渲染资源，在非渲染的数据结构上也有更多优化
     *
     * 具体请查看{@link  https://uniapp.dcloud.net.cn/component/recycle-list.html}
     */
    RecycleList: _RecycleList;
  }
}
