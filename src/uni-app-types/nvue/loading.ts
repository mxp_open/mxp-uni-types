/* eslint-disable @typescript-eslint/no-namespace */

import type { Component } from '../../utils/component';
import type { CustomEvent } from '../events';

/** 当 <scroll-view>、<list>、<waterfall> 被下拉完成时触发。 */
interface _LoadingOnLoading {
  (event: any): void;
}

interface _LoadingOnPullingdownDetail {
  /** 前后两次回调滑动距离的差值 */
  dy: number;
  /** 下拉的距离 */
  pullingDistance: number;
  /** Loading 组件高度 */
  viewHeight: number;
  /** 常数字符串 如“pullingdown” */
  type: string;
}

type _LoadingOnPullingdownEvent = CustomEvent<_LoadingOnPullingdownDetail>;
/** 当 <scroll-view>、<list>、<waterfall> 被下拉时触发。  */
interface _LoadingOnPullingdown {
  (event: _LoadingOnPullingdownEvent): void;
}

/** nvue专用 Loading 容器属性 */
type _LoadingProps = Partial<{
  /**
   * 控制 <Loading> 组件显示、隐藏。display 的设置必须成对出现，即设置 display="show", 必须有对应的 display="hide"。可选值为 show / hide，默认值为 show。
   */
  display: any;
  /**
   * <scroll-view>、<list>、<waterfall> 被下拉完成时触发。
   */
  onLoading: _LoadingOnLoading;

  onPullingdown: _LoadingOnPullingdown;
}>;

/**
 * app端nvue专用组件。<Loading> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
 *
 * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
 */
type _Loading = Component<_LoadingProps>;

/** nvue专用 Loading 容器实例 */
type _LoadingInstance = InstanceType<_Loading>;

export type { _LoadingProps as LoadingProps, _Loading as Loading, _LoadingInstance as LoadingInstance };

declare global {
  namespace GUni {
    /** nvue专用 Loading 容器属性 */
    export type LoadingProps = _LoadingProps;
    /**
     * app端nvue专用组件。<Loading> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
     *
     * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
     */
    export type Loading = _Loading;
    /** 视图容器实例 */
    export type LoadingInstance = _LoadingInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * app端nvue专用组件。<Loading> 为容器提供下拉刷新功能。在nvue上，可通过此组件实现灵活的、自定义的、高性能的下拉刷新。
     *
     * 具体请查看{@link https://uniapp.dcloud.net.cn/component/refresh.html}
     */
    Loading: _Loading;
  }
}
