/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../utils/component';
import type { CustomEvent } from '../events';

/** 剧集相关动作信息信息（点赞数、关注、运营位数据） */
type _PlayletConfigEntity = {
  /** 点赞相关 */
  like: {
    /** 是否已点赞 */
    liked: boolean;
    /** 点赞数 */
    likedCount: number;
  };
  /** 收藏相关 */
  collect: {
    /** 是否已收藏 */
    hasCollected: boolean;
  };
  /** 运营位相关 */
  operation: {
    /** 运营位图片 */
    operationUrl: string;
    /** 运营位描述 */
    operationDesc: string;
  };
  /** 关注相关 */
  follow: {
    /** 头像 */
    avatar: string;
    /** 是否已关注 */
    followed: boolean;
  };
};
/** 第三方剧集信息 */
type _PlayletSourceEntityEpisode = {
  [k: string]: any;
  /** 封面图片 */
  coverUrl: string;
  /** 简介 */
  introduction: string;
  /** 视频链接 */
  videoUrl: string;
  /** 视频时长 单位 秒(s) */
  duratoin: `${number}`;
  /** 第几集 */
  episodeNumber: number;
};

/** 三方cdn播放数据 */
type _PlayletSourceEntity = {
  [k: string]: any;
  /** 备案号 */
  filingNumber: string;
  /** 总集数 */
  totalEpisodeNumber: `${number}`;
  /** 第三方剧集信息 */
  episodeList: _PlayletSourceEntityEpisode[];
};

/** 扩展参数和三方cdn播放数据 */
type _PlayletExtParams = {
  [k: number | string]: any;
  /** 免费剧集list */
  freeList?: (number | string)[];
  /** 已付费剧集list */
  payedList?: (number | string)[];
  /** 某一集剧集相关信息,key为集数 （点赞数、关注、运营位数据） */
  configList?: Record<number, _PlayletConfigEntity>;
  /** 使用第三方 cdn 资源链接播放的数据 三方播放数据时必传 */
  sourceList?: _PlayletSourceEntity[];
};

/**
 * 动作类型
 * like 点赞
 * collect 收藏
 * follow 关注
 * operation 运营位点击
 */
type _PlayletOnActionDetailActionType = 'collect' | 'follow' | 'like' | 'operation';

/** 播放错误事件的event对象的详情 */
interface _PlayletOnErrorDetail {
  /** 错误code  */
  errorCode: number;
  /** 异常信息 */
  errorMsg: string;
}
/** 播放错误事件的event对象 */
type _PlayletOnErrorEvent = CustomEvent<_PlayletOnErrorDetail>;
/** 视频播放出错时触发 */
interface _PlayletOnError {
  (event: _PlayletOnErrorEvent): void;
}
/** 无权限播放事件的event对象的详情 */
interface _PlayletOnNopermissionplayDetail {
  /** 剧目id  */
  playletId: number;
  /** 当前剧集id */
  episodeNumber: number;
}
/** 无权限播放事件的event对象 */
type _PlayletOnNopermissionplayEvent = CustomEvent<_PlayletOnNopermissionplayDetail>;

/** 无权限播放时触发 (开发者需要针对此状态做对应的支付or 广告解锁操作操作完成后，更新 extParams.payedList 数据，从而完成短剧播放的履约部分) */
interface _PlayletOnNopermissionplay {
  (event: _PlayletOnNopermissionplayEvent): void;
}

/** 点击某个动作事件的event对象的详情 */
interface _PlayletOnActionDetail {
  /** 动作行为（如:点赞 true，去掉点赞 false）  */
  action: boolean;
  /** 动作类型 */
  actionType: _PlayletOnActionDetailActionType;
  /** 剧集id */
  episodeNumber: string;
}
/** 点击某个动作事件的event对象 */
type _PlayletOnActionEvent = CustomEvent<_PlayletOnActionDetail>;

/** 点击某个动作时触发 */
interface _PlayletOnAction {
  (event: _PlayletOnActionEvent): void;
}
/** 切换剧集事件的event对象的详情 */
interface _PlayletOnChangeDetail {
  /** 当前播放剧集id【使用内部资源时有】 */
  episodeId: string;
  /** 内容库时候写入的集数 [注意不建议用此作为下标] */
  playEpisodeNumber: number | string;
}
/** 切换剧集事件的event对象 */
type _PlayletOnChangeEvent = CustomEvent<_PlayletOnChangeDetail>;

/** 切换剧集时触发 */
interface _PlayletOnChange {
  (event: _PlayletOnChangeEvent): void;
}

/** 剧集播放结束事件的event对象的详情 */
interface _PlayletOnEndDetail {
  /** 当前播放剧集id【使用内部资源时有】 */
  episodeId: string;
  /** 内容库时候写入的集数 [注意不建议用此作为下标] */
  playEpisodeNumber: number | string;
}
/** 剧集播放结束事件的event对象 */
type _PlayletOnEndEvent = CustomEvent<_PlayletOnEndDetail>;

/** 剧集播放结束时触发 */
interface _PlayletOnEnd {
  (event: _PlayletOnEndEvent): void;
}

/** 剧集播放暂停事件的event对象的详情 */
interface _PlayletOnPauseDetail {
  /** 当前播放剧集id【使用内部资源时有】 */
  episodeId: string;
  /** 内容库时候写入的集数 [注意不建议用此作为下标] */
  playEpisodeNumber: number | string;
}
/** 剧集播放暂停事件的event对象 */
type _PlayletOnPauseEvent = CustomEvent<_PlayletOnPauseDetail>;

/** 剧集播放暂停时触发 */
interface _PlayletOnPause {
  (event: _PlayletOnPauseEvent): void;
}

/** 剧集开始播放事件的event对象的详情 */
interface _PlayletOnPlayDetail {
  /** 类型 "START_PLAY"： 开始播放；  "PLAY"：继续播放 */
  type: 'PLAY' | 'START_PLAY';
  /** 当前播放剧集id【使用内部资源时有】 */
  episodeId: string;
  /** 内容库时候写入的集数 [注意不建议用此作为下标] */
  playEpisodeNumber: number | string;
}
/** 剧集开始播放事件的event对象 */
type _PlayletOnPlayEvent = CustomEvent<_PlayletOnPlayDetail>;

/** 剧集开始播放时触发 */
interface _PlayletOnPlay {
  (event: _PlayletOnPlayEvent): void;
}

/** 视频播放组件属性 */
type _PlayletProps = Partial<{
  /** 剧目id(上传内容库返回的id) */
  playletId: string;
  /** 播放的剧集id */
  episodeIdList: string[];
  /** 指定播放剧集id或者是剧集数（剧集数只在三方cdn 生效）若不指定或未匹配到，则从第一集开始播放 */
  playId: number | string;
  /** 扩展参数和三方cdn播放数据 */
  extParams: _PlayletExtParams;
  /** 是否展示底部安全区 */
  showBottomSafeArea: boolean;
  /**	是否展示分享按钮 */
  showShare: boolean;
  /**	视频大小与 video 容器大小不一致时，视频的表现形式，参考video 的object-fit属性 */
  objectFit: 'contain' | 'cover' | 'fill';
  /**	播放倍率 */
  playbackRate: 0.5 | 0.75 | 1.0 | 1.5 | 1.25 | 2;
  /** 无权限播放时触发 (开发者需要针对此状态做对应的支付or 广告解锁操作操作完成后，更新 extParams.payedList 数据，从而完成短剧播放的履约部分) */
  onNopermissionplay: _PlayletOnNopermissionplay;
  /** 视频播放出错时触发 */
  onError: _PlayletOnError;
  /** 点击某个动作时触发 */
  onAction: _PlayletOnAction;
  /** 切换剧集时触发 */
  onChange: _PlayletOnChange;
  /** 播放结束时触发 */
  onEnd: _PlayletOnEnd;
  /** 暂停播放时触发 */
  onPause: _PlayletOnPause;
  /** 开始播放时触发 */
  onPlay: _PlayletOnPlay;
}>;

/** 视频播放组件 */
type _Playlet = Component<_PlayletProps>;

/** 视频播放组件实例 */
type _PlayletInstance = InstanceType<_Playlet>;

export type {
  _PlayletConfigEntity as PlayletConfigEntity,
  _PlayletSourceEntityEpisode as PlayletSourceEntityEpisode,
  _PlayletSourceEntity as PlayletSourceEntity,
  _PlayletExtParams as PlayletExtParams,
  _PlayletProps as PlayletProps,
  _Playlet as Playlet,
  _PlayletInstance as PlayletInstance,
  _PlayletOnActionDetailActionType as PlayletOnActionDetailActionType,
  _PlayletOnErrorDetail as PlayletOnErrorDetail,
  _PlayletOnErrorEvent as PlayletOnErrorEvent,
  _PlayletOnError as PlayletOnError,
  _PlayletOnNopermissionplayDetail as PlayletOnNopermissionplayDetail,
  _PlayletOnNopermissionplayEvent as PlayletOnNopermissionplayEvent,
  _PlayletOnNopermissionplay as PlayletOnNopermissionplay,
  _PlayletOnActionDetail as PlayletOnActionDetail,
  _PlayletOnActionEvent as PlayletOnActionEvent,
  _PlayletOnAction as PlayletOnAction,
  _PlayletOnChangeDetail as PlayletOnChangeDetail,
  _PlayletOnChangeEvent as PlayletOnChangeEvent,
  _PlayletOnChange as PlayletOnChange,
  _PlayletOnEndDetail as PlayletOnEndDetail,
  _PlayletOnEndEvent as PlayletOnEndEvent,
  _PlayletOnEnd as PlayletOnEnd,
  _PlayletOnPauseDetail as PlayletOnPauseDetail,
  _PlayletOnPauseEvent as PlayletOnPauseEvent,
  _PlayletOnPause as PlayletOnPause,
  _PlayletOnPlayDetail as PlayletOnPlayDetail,
  _PlayletOnPlayEvent as PlayletOnPlayEvent,
  _PlayletOnPlay as PlayletOnPlay,
};

declare global {
  namespace GUni {
    /** 剧集相关动作信息信息（点赞数、关注、运营位数据） */
    export type PlayletConfigEntity = _PlayletConfigEntity;
    /** 第三方剧集信息 */
    export type PlayletSourceEntityEpisode = _PlayletSourceEntityEpisode;
    /** 三方cdn播放数据 */
    export type PlayletSourceEntity = _PlayletSourceEntity;
    /** 扩展参数和三方cdn播放数据 */
    export type PlayletExtParams = _PlayletExtParams;
    /** 视频播放组件属性 */
    export type PlayletProps = _PlayletProps;
    /** 抖音短剧专用视频播放组件 */
    export type Playlet = _Playlet;
    /** 视频播放组件实例 */
    export type PlayletInstance = _PlayletInstance;

    /** 动作类型 like 点赞 collect 收藏 follow 关注 operation 运营位点击 */
    export type PlayletOnActionDetailActionType = _PlayletOnActionDetailActionType;
    /** 播放错误事件的event对象的详情 */
    export type PlayletOnErrorDetail = _PlayletOnErrorDetail;
    /** 播放错误事件的event对象 */
    export type PlayletOnErrorEvent = _PlayletOnErrorEvent;
    /** 视频播放出错时触发 */
    export type PlayletOnError = _PlayletOnError;
    /** 无权限播放事件的event对象的详情 */
    export type PlayletOnNopermissionplayDetail = _PlayletOnNopermissionplayDetail;
    /** 无权限播放事件的event对象 */
    export type PlayletOnNopermissionplayEvent = _PlayletOnNopermissionplayEvent;
    /** 无权限播放时触发 (开发者需要针对此状态做对应的支付or 广告解锁操作操作完成后，更新 extParams.payedList 数据，从而完成短剧播放的履约部分) */
    export type PlayletOnNopermissionplay = _PlayletOnNopermissionplay;
    /** 点击某个动作事件的event对象的详情 */
    export type PlayletOnActionDetail = _PlayletOnActionDetail;
    /** 点击某个动作事件的event对象 */
    export type PlayletOnActionEvent = _PlayletOnActionEvent;
    /** 点击某个动作时触发 */
    export type PlayletOnAction = _PlayletOnAction;
    /** 切换剧集事件的event对象的详情 */
    export type PlayletOnChangeDetail = _PlayletOnChangeDetail;
    /** 切换剧集事件的event对象 */
    export type PlayletOnChangeEvent = _PlayletOnChangeEvent;
    /** 切换剧集时触发 */
    export type PlayletOnChange = _PlayletOnChange;
    /** 剧集播放结束事件的event对象的详情 */
    export type PlayletOnEndDetail = _PlayletOnEndDetail;
    /** 剧集播放结束事件的event对象 */
    export type PlayletOnEndEvent = _PlayletOnEndEvent;
    /** 剧集播放结束时触发 */
    export type PlayletOnEnd = _PlayletOnEnd;
    /** 剧集播放暂停事件的event对象的详情 */
    export type PlayletOnPauseDetail = _PlayletOnPauseDetail;
    /** 剧集播放暂停事件的event对象 */
    export type PlayletOnPauseEvent = _PlayletOnPauseEvent;
    /** 剧集播放暂停时触发 */
    export type PlayletOnPause = _PlayletOnPause;
    /** 剧集开始播放事件的event对象的详情 */
    export type PlayletOnPlayDetail = _PlayletOnPlayDetail;
    /** 剧集开始播放事件的event对象 */
    export type PlayletOnPlayEvent = _PlayletOnPlayEvent;
    /** 剧集开始播放时触发 */
    export type PlayletOnPlay = _PlayletOnPlay;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * 抖音短剧专用视频播放组件
     */
    Playlet: _Playlet;
  }
}
