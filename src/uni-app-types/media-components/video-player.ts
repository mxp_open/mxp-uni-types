/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../utils/component';
import type { BaseEvent, CustomEvent } from '../events';

/**
 * 使用cdn类型，1: 抖音云   2: 三方云。
 */
type _VideoPlayerCloudType = 1 | 2;

/**
 * 设置全屏时视频的方向，不指定则根据宽高比自动判断
 *
 * 0 正常竖向
 *
 * 90 屏幕逆时针 90 度
 *
 * -90 屏幕顺时针 90 度
 */
type _VideoPlayerDirection = -90 | 0 | 90;

/**
 * 当视频大小与 video 容器大小不一致时，视频的表现形式
 *
 * contain 包含
 *
 * fill 填充
 *
 * cover 覆盖
 */
type _VideoPlayerObjectFit = 'contain' | 'cover' | 'fill';

/**
 * 视频的封面图片大小与 video 容器大小不一致时，封面图片的表现形式。
 *
 * contain 包含
 *
 * fill 填充
 *
 * cover 覆盖
 */
type _VideoPlayerPosterSize = 'contain' | 'cover' | 'fill';

/**
 * 播放按钮的位置
 *
 * bottom 控制栏上
 *
 * center 视频中间
 */
type _VideoPlayerPlayBtnPosition = 'bottom' | 'center';

/**
 * 水印署名水印位置
 * 0（右上）
 * 1（右下）
 * 2（左下）
 * 3（左上）
 */
type _VideoPlayerSignaturePosition = 0 | 1 | 2 | 4;

/** 水印 */
type _VideoPlayerSignature = {
  /**
   * 是否显示署名水印
   */
  enable: boolean;
  /**
   * 署名水印内容
   */
  content: string;
  /**
   * 署名水印位置
   * 0（右上）
   * 1（右下）
   * 2（左下）
   * 3（左上）
   */
  position: _VideoPlayerSignaturePosition;
  /**
   * 署名水印颜色，只支持 HEX 格式。
   * 默认值 #FFFFFF。
   */
  color: string;
};

type _VideoPlayerOnPlayEvent = BaseEvent;

/** 开始/继续播放时触发 */
interface _VideoPlayerOnPlay {
  (event: _VideoPlayerOnPlayEvent): void;
}

type _VideoPlayerOnPauseEvent = BaseEvent;

/** 暂停播放时触发 */
interface _VideoPlayerOnPause {
  (event: _VideoPlayerOnPauseEvent): void;
}

type _VideoPlayerOnEndedEvent = BaseEvent;

/** 播放到末尾时触发 */
interface _VideoPlayerOnEnded {
  (event: _VideoPlayerOnEndedEvent): void;
}

type _VideoPlayerOnErrorEvent = BaseEvent;

/** 视频播放出错时触发 */
interface _VideoPlayerOnError {
  (event: _VideoPlayerOnErrorEvent): void;
}

interface _VideoPlayerOnTimeupdateDetail {
  /** 当前播放时间，单位 s */
  currentTime: number;
  /** 视频总时长，单位 s */
  duration: number;
}

type _VideoPlayerOnTimeupdateEvent = CustomEvent<_VideoPlayerOnTimeupdateDetail>;

/**
 * 播放进度变化时触发
 *
 */
interface _VideoPlayerOnTimeupdate {
  (event: _VideoPlayerOnTimeupdateEvent): void;
}

interface _VideoPlayerOnProgressDetail {
  /** 视频缓冲进度百分比，取值是0到100中的整数，如 buffered 为 50 表示当前视频缓冲了 50%  */
  buffered: number;
}

type _VideoPlayerOnProgressEvent = CustomEvent<_VideoPlayerOnProgressDetail>;

/** 加载进度变化时触发 */
interface _VideoPlayerOnProgress {
  (event: _VideoPlayerOnProgressEvent): void;
}

interface _VideoPlayerOnFullscreenchangeDetail {
  /** 是否为全屏 */
  fullScreen: boolean;
  /** 视频方向 仅在 fullScreen 字段为 true 时存在，有效值为 vertical 或 horizontal，表示纵向和横向 */
  direction: 'horizontal' | 'vertical';
}

type _VideoPlayerOnFullscreenchangeEvent = CustomEvent<_VideoPlayerOnFullscreenchangeDetail>;

/** 视频进入和退出全屏时触发 */
interface _VideoPlayerOnFullscreenchange {
  (event: _VideoPlayerOnFullscreenchangeEvent): void;
}

type _VideoPlayerOnLoadeddataEvent = BaseEvent;

interface _VideoPlayerOnFullscreenclickDetail {
  screenX: number;
  screenY: number;
  screenWidth: number;
  screenHeight: number;
}

type _VideoPlayerOnFullscreenclickEvent = CustomEvent<_VideoPlayerOnFullscreenclickDetail>;

interface _VideoPlayerOnControlstoggleDetail {
  /** 显示或隐藏 */
  show: boolean;
}

type _VideoPlayerOnControlstoggleEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;

/** 切换 controls 显示隐藏时触发 */
interface _VideoPlayerOnControlstoggle {
  (event: _VideoPlayerOnControlstoggleEvent): void;
}

type _VideoPlayerOnWaitingEvent = BaseEvent;

/** 视频缓冲时触发 */
interface _VideoPlayerOnWaiting {
  (event: _VideoPlayerOnWaitingEvent): void;
}

interface _VideoPlayerOnLoadedmetadataDetail {
  /** 视频宽度 */
  width: number;
  /** 视频高度 */
  height: number;
  /** 视频总时长 单位 秒(s) */
  duration: number;
}

type _VideoPlayerOnLoadedmetadataEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;
/** 视频元数据加载完成时触发 */
interface _VideoPlayerOnLoadedmetadata {
  (event: _VideoPlayerOnLoadedmetadataEvent): void;
}

interface _VideoPlayerOnSeekcompleteDetail {
  /** 拖拽进度完成后的播放时间点 单位 秒(s) */
  position: number;
}

type _VideoPlayerOnSeekcompleteEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;
/** 拖拽进度完成后触发 */
interface _VideoPlayerOnSeekcomplete {
  (event: _VideoPlayerOnSeekcompleteEvent): void;
}

interface _VideoPlayerOnPlaybackratechangeDetail {
  /** 改变后的倍速值 */
  playbackRate: number;
}

type _VideoPlayerOnPlaybackratechangeEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;
/** 视频倍速改变完成时触发 */
interface _VideoPlayerOnPlaybackratechange {
  (event: _VideoPlayerOnPlaybackratechangeEvent): void;
}

interface _VideoPlayerOnMutechangeDetail {
  /** 当前是否静音 */
  isMuted: boolean;
}

type _VideoPlayerOnMutechangeEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;
/** 静音状态改变完成时触发 */
interface _VideoPlayerOnMutechange {
  (event: _VideoPlayerOnSeekcompleteEvent): void;
}

/**
 * play 播放控件 / 重播控件 (不包括点击视频初始状态的播放按钮)
 * pause 暂停控件
 * fullscreen  全屏控件
 * mute  静音控件
 * playbackRate  倍速控件（不包括倍速选择面板）
 * progress  进度控件，在刚开始拖动进度条时触发
 */
type _VideoPlayerOnControltapDetailControlType = 'fullscreen' | 'mute' | 'pause' | 'play' | 'playbackRate' | 'progress';

interface _VideoPlayerOnControltapDetail {
  /** 当前点击的控件类型 */
  controlType: _VideoPlayerOnControltapDetailControlType;
}

type _VideoPlayerOnControltapEvent = CustomEvent<_VideoPlayerOnControlstoggleDetail>;
/** 静音状态改变完成时触发 */
interface _VideoPlayerOnControltap {
  (event: _VideoPlayerOnSeekcompleteEvent): void;
}

type _VideoPlayerOnEnterbackgroundEvent = BaseEvent;
/** 进入小窗播放时触发。 */
interface _VideoPlayerOnEnterbackground {
  (event: _VideoPlayerOnEnterbackgroundEvent): void;
}

type _VideoPlayerOnClosebackgroundEvent = BaseEvent;
/** 关闭小窗播放时触发。 */
interface _VideoPlayerOnClosebackground {
  (event: _VideoPlayerOnClosebackgroundEvent): void;
}

type _VideoPlayerOnLeavebackgroundEvent = BaseEvent;
/** 离开小窗进入 app 事件时触发 */
interface _VideoPlayerOnLeavebackground {
  (event: _VideoPlayerOnLeavebackgroundEvent): void;
}

type _VideoPlayerOnGetsourceEvent = BaseEvent;
/** 获取资源完成后触发 */
interface _VideoPlayerOnGetsource {
  (event: _VideoPlayerOnGetsourceEvent): void;
}

/** 视频播放组件属性 */
type _VideoPlayerProps = Partial<{
  /** 原片id */
  albumId: string;
  /** 剧集id */
  episodeId: string;
  /**
   * 使用cdn类型，1: 抖音云   2: 三方云。
   * 可用 tt.canIUse('video-player.cloudType') 判断是否支持该属性。
   */
  cloudType: _VideoPlayerCloudType;
  /** 三方云链接，当cloud_type为2，即三方云时必传 */
  threePartyCloud: string;
  /** 废弃字段。为了兼容老版本，请传 1 。 */
  version: '1';
  /**
   * 是否自动播放
   *
   * 默认为 false
   */
  autoplay: boolean;
  /**
   * 视频封面的图片网络资源地址
   *
   * 如果 controls 值为 false 则无效
   */
  poster: string;
  /**
   * 是否循环播放
   *
   * 默认为 false
   */
  loop: boolean;
  /**
   * 是否显示全屏按钮
   *
   * 默认为 true
   */
  showFullscreenBtn: boolean;
  /**
   * 是否显示播放、暂停、重播按钮，不包括视频封面的播放按钮。
   *
   * 默认为 true
   */
  showPlayBtn: boolean;
  /**
   * 是否显示全部播放控件。 （如播放/暂停按钮、播放进度、时间）
   *
   * 默认为 true
   */
  controls: boolean;
  /**
   * 当视频大小与 video 容器大小不一致时，视频的表现形式
   *
   * contain 包含
   *
   * fill 填充
   *
   * cover 覆盖
   *
   * 默认为 contain
   */
  objectFit: _VideoPlayerObjectFit;
  /**
   * 播放按钮的位置
   *
   * bottom 控制栏上
   *
   * center 视频中间
   *
   * 默认为 bottom
   */
  playBtnPosition: _VideoPlayerPlayBtnPosition;

  /**
   * 在非全屏模式下，是否开启亮度与音量调节手势，开启后表现详见手势响应-亮度与音量。
   *
   * 默认为 false
   */
  vslideGesture: boolean;
  /**
   * 在全屏模式下，是否开启亮度与音量调节手势，开启后表现详见手势响应-亮度与音量。
   *
   * 默认为 true
   */
  vslideGestureInFullscreen: boolean;
  /**
   * 是否开启控制进度的手势，开启后表现详见手势响应-播放进度。
   *
   * 默认为 true
   */
  enableProgressGesture: boolean;
  /**
   * 是否开启播放手势，即双击切换播放/暂停。
   * 默认为 false
   */
  enablePlayGesture: boolean;
  /**
   * 是否静音播放
   *
   * 默认为 false
   */
  muted: boolean;

  /**
   * 是否显示静音控件，仅在全屏时显示。
   * 默认为 false
   */
  showMuteBtn: boolean;

  /**
   * 是否显示倍速控件，仅在全屏时显示。点击倍速控件后可选择倍速，可选值： 0.75/1.0/1.25/1.5/2。
   * 默认为 false
   */
  showPlaybackRateBtn: boolean;
  /**
   * 设置全屏时视频的方向，不指定则根据宽高比自动判断
   *
   * 0 正常竖向
   *
   * 90 屏幕逆时针 90 度
   *
   * -90 屏幕顺时针 90 度
   *
   * 默认根据宽高比自动判断
   */
  direction: _VideoPlayerDirection;

  /**
   * video 播放时宿主退出后台后开启小窗播放，iOS 14 及以上版本支持。开启时首次退出后台后给予弹窗提示用户授权，授权完成后可以到小程序「设置」中重设。支持场景见后台小窗播放。
   * 默认为 false
   */
  enablePlayInBackground: boolean;

  /**
   * 设置署名水印，属性说明详见 Signature 类型说明。
   */
  signature: _VideoPlayerSignature;

  /**
   * 指定视频初始播放位置
   *
   * 单位为 s
   */
  initialTime: number;
  /**
   * 是否展示锁屏按钮，仅在全屏时展示，锁屏后会锁定播控/手势的操作。
   * 默认为 false
   */
  showScreenLockButton: boolean;
  /**
   * 是否展示中间的播放进度条。
   *
   * 默认为 true
   */
  showProgress: boolean;
  /**
   * 是否展示底部的播放进度条。
   *
   * 默认为 true
   */
  showBottomProgress: boolean;
  /**
   * 视频的封面图片大小与 video 容器大小不一致时，封面图片的表现形式。
   *
   * contain 包含
   *
   * fill 填充
   *
   * cover 覆盖
   *
   * 默认为 contain
   */
  posterSize: _VideoPlayerPosterSize;
  /**
   * 限制视频的最大可播放时长，用于试看等场景。
   */
  durationIimit: number;
  /**
   * 是否开启界面暗水印功能，用于视频防盗录场景。暗水印解码效果需要根据实际情况而定。小程序使用了暗水印能力后出现的盗录场景，可以拉客服咨询。
   */
  enableDarkWaterMark: boolean;

  /**
   * 目前暂不支持
   * 对视频内容的传输过程进行加密，通过 encrypted-token 设置通用加密（CENC）的解密 key，加密流程见 FFmpeg通用加密:https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/guide/open-capabilities/encrypted-token。
   */
  encryptedToken: string;

  /** 开始/继续播放时触发 */
  onPlay: _VideoPlayerOnPlay;
  /** 暂停播放时触发 */
  onPause: _VideoPlayerOnPause;
  /** 播放到末尾时触发 */
  onEnded: _VideoPlayerOnEnded;
  /** 视频播放出错时触发 */
  onError: _VideoPlayerOnError;
  /**
   * 播放进度变化时触发，返回当前播放时间点及视频总时长，单位：秒(s)。event.detail = { currentTime, duration }。
   */
  onTimeupdate: _VideoPlayerOnTimeupdate;
  /** 视频缓冲进度更新时触发，event.detail = { buffered }。其中 buffered 是百分比，取值是 [0, 100] 中的整数，如 buffered 为 50 表示当前视频缓冲了 50%  */
  onProgress: _VideoPlayerOnProgress;
  /** 视频进入和退出全屏时触发，event.detail = { fullScreen, direction}，其中 direction 仅在 fullScreen 字段为 true 时存在，有效值为 vertical 或 horizontal。  */
  onFullscreenchange: _VideoPlayerOnFullscreenchange;
  /** 切换播放控件显示/隐藏时触发。event.detail = { show }。 */
  onControlstoggle: _VideoPlayerOnControlstoggle;
  /** 视频缓冲时触发 */
  onWaiting: _VideoPlayerOnWaiting;
  /** 视频元数据加载完成时触发。event.detail = {width, height, duration}。  */
  onLoadedmetadata: _VideoPlayerOnLoadedmetadata;

  /** 拖拽进度 完成时触发。返回 seek 完成后的播放时间点，单位：秒(s)。event.detail={position}。 */
  onSeekcomplete: _VideoPlayerOnSeekcomplete;
  /** 静音状态改变完成时触发。返回当前是否静音。event.detail={isMuted} */
  onMutechange: _VideoPlayerOnMutechange;
  /** 点击控件时触发。返回当前点击的控件类型。event.detail={controlType}，取值见表 controlType 的合法值。  */
  onControltap: _VideoPlayerOnControltap;
  /**
   * 进入小窗播放时触发。
   */
  onEnterbackground: _VideoPlayerOnEnterbackground;
  /**
   * 关闭小窗播放时触发。
   */
  onClosebackground: _VideoPlayerOnClosebackground;
  /**
   * 离开小窗进入 app 事件时触发。
   */
  onLeavebackground: _VideoPlayerOnLeavebackground;
  /**
   * 获取资源完成后触发。可在这个时机后用 api 播放时。
   * 可用 tt.canIUse('video-player.bindgetsource') 校验是否支持该事件。
   */
  onGetsource: _VideoPlayerOnGetsource;
}>;

/**
 * 视频播放组件
 */
type _VideoPlayer = Component<_VideoPlayerProps>;

/** 视频播放组件实例 */
type _VideoPlayerInstance = InstanceType<_VideoPlayer>;

export type {
  _VideoPlayerPosterSize,
  _VideoPlayerSignature as VideoPlayerSignature,
  _VideoPlayerSignaturePosition as VideoPlayerSignaturePosition,
  _VideoPlayerCloudType as VideoPlayerCloudType,
  _VideoPlayerDirection as VideoPlayerDirection,
  _VideoPlayerObjectFit as VideoPlayerObjectFit,
  _VideoPlayerPlayBtnPosition as VideoPlayerPlayBtnPosition,
  _VideoPlayerOnPlayEvent as VideoPlayerOnPlayEvent,
  _VideoPlayerOnPlay as VideoPlayerOnPlay,
  _VideoPlayerOnPauseEvent as VideoPlayerOnPauseEvent,
  _VideoPlayerOnPause as VideoPlayerOnPause,
  _VideoPlayerOnEndedEvent as VideoPlayerOnEndedEvent,
  _VideoPlayerOnEnded as VideoPlayerOnEnded,
  _VideoPlayerOnTimeupdateDetail as VideoPlayerOnTimeupdateDetail,
  _VideoPlayerOnTimeupdateEvent as VideoPlayerOnTimeupdateEvent,
  _VideoPlayerOnTimeupdate as VideoPlayerOnTimeupdate,
  _VideoPlayerOnFullscreenchangeDetail as VideoPlayerOnFullscreenchangeDetail,
  _VideoPlayerOnFullscreenchangeEvent as VideoPlayerOnFullscreenchangeEvent,
  _VideoPlayerOnFullscreenchange as VideoPlayerOnFullscreenchange,
  _VideoPlayerOnWaitingEvent as VideoPlayerOnWaitingEvent,
  _VideoPlayerOnWaiting as VideoPlayerOnWaiting,
  _VideoPlayerOnErrorEvent as VideoPlayerOnErrorEvent,
  _VideoPlayerOnError as VideoPlayerOnError,
  _VideoPlayerOnProgressDetail as VideoPlayerOnProgressDetail,
  _VideoPlayerOnProgressEvent as VideoPlayerOnProgressEvent,
  _VideoPlayerOnProgress as VideoPlayerOnProgress,
  _VideoPlayerOnLoadeddataEvent as VideoPlayerOnLoadeddataEvent,
  _VideoPlayerOnLoadedmetadataDetail as VideoPlayerOnLoadedmetadataDetail,
  _VideoPlayerOnLoadedmetadataEvent as VideoPlayerOnLoadedmetadataEvent,
  _VideoPlayerOnLoadedmetadata as VideoPlayerOnLoadedmetadata,
  _VideoPlayerOnFullscreenclickDetail as VideoPlayerOnFullscreenclickDetail,
  _VideoPlayerOnFullscreenclickEvent as VideoPlayerOnFullscreenclickEvent,
  _VideoPlayerOnControlstoggleDetail as VideoPlayerOnControlstoggleDetail,
  _VideoPlayerOnControlstoggleEvent as VideoPlayerOnControlstoggleEvent,
  _VideoPlayerOnControlstoggle as VideoPlayerOnControlstoggle,
  _VideoPlayerProps as VideoPlayerProps,
  _VideoPlayer as VideoPlayer,
  _VideoPlayerInstance as VideoPlayerInstance,
  _VideoPlayerOnSeekcompleteDetail as VideoPlayerOnSeekcompleteDetail,
  _VideoPlayerOnSeekcompleteEvent as VideoPlayerOnSeekcompleteEvent,
  _VideoPlayerOnSeekcomplete as VideoPlayerOnSeekcomplete,
  _VideoPlayerOnPlaybackratechangeDetail as VideoPlayerOnPlaybackratechangeDetail,
  _VideoPlayerOnPlaybackratechangeEvent as VideoPlayerOnPlaybackratechangeEvent,
  _VideoPlayerOnPlaybackratechange as VideoPlayerOnPlaybackratechange,
  _VideoPlayerOnMutechangeDetail as VideoPlayerOnMutechangeDetail,
  _VideoPlayerOnMutechangeEvent as VideoPlayerOnMutechangeEvent,
  _VideoPlayerOnMutechange as VideoPlayerOnMutechange,
  _VideoPlayerOnControltapDetailControlType as VideoPlayerOnControltapDetailControlType,
  _VideoPlayerOnControltapDetail as VideoPlayerOnControltapDetail,
  _VideoPlayerOnControltapEvent as VideoPlayerOnControltapEvent,
  _VideoPlayerOnControltap as VideoPlayerOnControltap,
  _VideoPlayerOnEnterbackgroundEvent as VideoPlayerOnEnterbackgroundEvent,
  _VideoPlayerOnEnterbackground as VideoPlayerOnEnterbackground,
  _VideoPlayerOnClosebackgroundEvent as VideoPlayerOnClosebackgroundEvent,
  _VideoPlayerOnClosebackground as VideoPlayerOnClosebackground,
  _VideoPlayerOnLeavebackgroundEvent as VideoPlayerOnLeavebackgroundEvent,
  _VideoPlayerOnLeavebackground as VideoPlayerOnLeavebackground,
  _VideoPlayerOnGetsourceEvent as VideoPlayerOnGetsourceEvent,
  _VideoPlayerOnGetsource as VideoPlayerOnGetsource,
};

declare global {
  namespace GUni {
    /**
     * 使用cdn类型，1: 抖音云   2: 三方云。
     */
    export type VideoPlayerCloudType = _VideoPlayerCloudType;
    /**
     * 设置全屏时视频的方向，不指定则根据宽高比自动判断
     *
     * 0 正常竖向
     *
     * 90 屏幕逆时针 90 度
     *
     * -90 屏幕顺时针 90 度
     */
    export type VideoPlayerDirection = _VideoPlayerDirection;
    /**
     * 当视频大小与 video 容器大小不一致时，视频的表现形式
     *
     * contain 包含
     *
     * fill 填充
     *
     * cover 覆盖
     */
    export type VideoPlayerObjectFit = _VideoPlayerObjectFit;
    /**
     * 播放按钮的位置
     *
     * bottom 控制栏上
     *
     * center 视频中间
     */
    export type VideoPlayerPlayBtnPosition = _VideoPlayerPlayBtnPosition;

    /**
     * 播放策略
     *
     * 0 普通模式，适合绝大部分视频播放场景
     *
     * 1 平滑播放模式（降级），增加缓冲区大小，采用 open sl 解码音频，避免音视频脱轨的问题，可能会降低首屏展现速度、视频帧率，出现开屏音频延迟等，适用于高码率视频的极端场景
     *
     * 2 M3U8 优化模式，增加缓冲区大小，提升视频加载速度和流畅度，可能会降低首屏展现速度，适用于 M3U8 在线播放的场景
     */
    export type VideoPlayerOnPlayEvent = _VideoPlayerOnPlayEvent;
    /** 开始/继续播放时触发 */
    export type VideoPlayerOnPlay = _VideoPlayerOnPlay;
    export type VideoPlayerOnPauseEvent = _VideoPlayerOnPauseEvent;
    /** 暂停播放时触发 */
    export type VideoPlayerOnPause = _VideoPlayerOnPause;
    export type VideoPlayerOnEndedEvent = _VideoPlayerOnEndedEvent;
    /** 播放到末尾时触发 */
    export type VideoPlayerOnEnded = _VideoPlayerOnEnded;
    export type VideoPlayerOnTimeupdateDetail = _VideoPlayerOnTimeupdateDetail;
    export type VideoPlayerOnTimeupdateEvent = _VideoPlayerOnTimeupdateEvent;
    /**
     * 播放进度变化时触发，
     *
     * 250ms 一次
     */
    export type VideoPlayerOnTimeupdate = _VideoPlayerOnTimeupdate;
    export type VideoPlayerOnFullscreenchangeDetail = _VideoPlayerOnFullscreenchangeDetail;
    export type VideoPlayerOnFullscreenchangeEvent = _VideoPlayerOnFullscreenchangeEvent;
    /** 视频进入和退出全屏时触发 */
    export type VideoPlayerOnFullscreenchange = _VideoPlayerOnFullscreenchange;
    export type VideoPlayerOnWaitingEvent = _VideoPlayerOnWaitingEvent;
    /** 视频缓冲时触发 */
    export type VideoPlayerOnWaiting = _VideoPlayerOnWaiting;
    export type VideoPlayerOnErrorEvent = _VideoPlayerOnErrorEvent;
    /** 视频播放出错时触发 */
    export type VideoPlayerOnError = _VideoPlayerOnError;
    export type VideoPlayerOnProgressDetail = _VideoPlayerOnProgressDetail;
    export type VideoPlayerOnProgressEvent = _VideoPlayerOnProgressEvent;
    /** 加载进度变化时触发 */
    export type VideoPlayerOnProgress = _VideoPlayerOnProgress;
    export type VideoPlayerOnLoadeddataEvent = _VideoPlayerOnLoadeddataEvent;

    /** 拖动进度条时触发 */
    export type VideoPlayerOnLoadedmetadataDetail = _VideoPlayerOnLoadedmetadataDetail;
    export type VideoPlayerOnLoadedmetadataEvent = _VideoPlayerOnLoadedmetadataEvent;
    /** 视频元数据加载完成时触发 */
    export type VideoPlayerOnLoadedmetadata = _VideoPlayerOnLoadedmetadata;
    export type VideoPlayerOnFullscreenclickDetail = _VideoPlayerOnFullscreenclickDetail;
    export type VideoPlayerOnFullscreenclickEvent = _VideoPlayerOnFullscreenclickEvent;
    /** 视频播放全屏播放点击时触发 */
    export type VideoPlayerOnControlstoggleDetail = _VideoPlayerOnControlstoggleDetail;
    export type VideoPlayerOnControlstoggleEvent = _VideoPlayerOnControlstoggleEvent;
    /** 切换 controls 显示隐藏时触发 */
    export type VideoPlayerOnControlstoggle = _VideoPlayerOnControlstoggle;
    /** 视频播放组件属性 */
    export type VideoPlayerProps = _VideoPlayerProps;
    /**
     * 抖音短剧专用视频播放组件
     *
     * 默认宽度 300px、高度 225px，可通过 css 设置宽高
     */
    export type VideoPlayer = _VideoPlayer;
    /** 视频播放组件实例 */
    export type VideoPlayerInstance = _VideoPlayerInstance;
    /**
     * 水印
     */
    export type VideoPlayerSignature = _VideoPlayerSignature;
    /**
     * 水印署名水印位置
     * 0（右上）
     * 1（右下）
     * 2（左下）
     * 3（左上）
     */
    export type VideoPlayerSignaturePosition = _VideoPlayerSignaturePosition;

    /**
     * 视频的封面图片大小与 video 容器大小不一致时，封面图片的表现形式。
     * contain（包含）
     * fill（填充）
     * cover（覆盖）
     */
    export type VideoPlayerPosterSize = _VideoPlayerPosterSize;

    export type VideoPlayerOnSeekcompleteDetail = _VideoPlayerOnSeekcompleteDetail;
    export type VideoPlayerOnSeekcompleteEvent = _VideoPlayerOnSeekcompleteEvent;
    export type VideoPlayerOnSeekcomplete = _VideoPlayerOnSeekcomplete;
    export type VideoPlayerOnPlaybackratechangeDetail = _VideoPlayerOnPlaybackratechangeDetail;
    export type VideoPlayerOnPlaybackratechangeEvent = _VideoPlayerOnPlaybackratechangeEvent;
    export type VideoPlayerOnPlaybackratechange = _VideoPlayerOnPlaybackratechange;
    export type VideoPlayerOnMutechangeDetail = _VideoPlayerOnMutechangeDetail;
    export type VideoPlayerOnMutechangeEvent = _VideoPlayerOnMutechangeEvent;
    export type VideoPlayerOnMutechange = _VideoPlayerOnMutechange;
    /** 当前点击的控件类型 */
    export type VideoPlayerOnControltapDetailControlType = _VideoPlayerOnControltapDetailControlType;
    export type VideoPlayerOnControltapDetail = _VideoPlayerOnControltapDetail;
    export type VideoPlayerOnControltapEvent = _VideoPlayerOnControltapEvent;
    export type VideoPlayerOnControltap = _VideoPlayerOnControltap;
    export type VideoPlayerOnEnterbackgroundEvent = _VideoPlayerOnEnterbackgroundEvent;
    export type VideoPlayerOnEnterbackground = _VideoPlayerOnEnterbackground;
    export type VideoPlayerOnClosebackgroundEvent = _VideoPlayerOnClosebackgroundEvent;
    export type VideoPlayerOnClosebackground = _VideoPlayerOnClosebackground;
    export type VideoPlayerOnLeavebackgroundEvent = _VideoPlayerOnLeavebackgroundEvent;
    export type VideoPlayerOnLeavebackground = _VideoPlayerOnLeavebackground;
    export type VideoPlayerOnGetsourceEvent = _VideoPlayerOnGetsourceEvent;
    export type VideoPlayerOnGetsource = _VideoPlayerOnGetsource;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * 抖音短剧专用视频播放组件
     */
    VideoPlayer: _VideoPlayer;
  }
}
