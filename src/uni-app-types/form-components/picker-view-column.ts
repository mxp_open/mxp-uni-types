/* eslint-disable @typescript-eslint/no-namespace */
import type { AnyRecord, Component } from '../../utils/component';

type _PickerViewColumnProps = Partial<AnyRecord>;

type _PickerViewColumn = Component<_PickerViewColumnProps>;

type _PickerViewColumnInstance = InstanceType<_PickerViewColumn>;

export type { _PickerViewColumnProps as PickerViewColumnProps, _PickerViewColumn as PickerViewColumn, _PickerViewColumnInstance as PickerViewColumnInstance };

declare global {
  namespace GUni {
    export type PickerViewColumnProps = _PickerViewColumnProps;
    export type PickerViewColumn = _PickerViewColumn;
    export type PickerViewColumnInstance = _PickerViewColumnInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    PickerViewColumn: _PickerViewColumn;
  }
}
