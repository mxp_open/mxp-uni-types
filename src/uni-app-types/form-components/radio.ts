/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../utils/component';

/**
 * 标识
 *
 * 被选中时，radio-group 的 change 事件会携带该 value
 */
type _RadioValue = string;

/** 单选项目属性 */
type _RadioProps = Partial<{
  /** 在 form 中作为 key */
  name: string;
  /**
   * 标识
   *
   * 被选中时，radio-group 的 change 事件会携带该 value
   */
  value: _RadioValue;
  /**
   * 当前是否选中
   *
   * 默认为 false
   */
  checked: boolean;
  /**
   * 是否禁用
   *
   * 默认为 false
   */
  disabled: boolean;

  /**
   * radio的颜色
   */
  color: string;

  /**
   * radio默认的背景颜色 仅 H5(3.99+)、App-Vue(3.99+) 支持
   * 默认为 #007AFF
   */
  backgroundColor: string;
  /**
   * radio默认的边框颜色 仅 H5(3.99+)、App-Vue(3.99+) 支持
   */
  borderColor: string;

  /**
   * radio选中时的背景颜色，优先级大于color属性 仅 H5(3.99+)、App-Vue(3.99+) 支持
   * 默认为 #007AFF
   */
  activeBackgroundColor: string;
  /**
   * radio选中时的边框颜色 仅 H5(3.99+)、App-Vue(3.99+) 支持
   */
  activeBorderColor: string;
  /**
   * radio的图标颜色 仅 H5(3.99+)、App-Vue(3.99+) 支持
   *
   * 默认为 #ffffff
   */
  iconColor: string;
}>;

/** 单选项目 */
type _Radio = Component<_RadioProps>;

/** 单选项目实例 */
type _RadioInstance = InstanceType<_Radio>;

export type { _RadioValue as RadioValue, _RadioProps as RadioProps, _Radio as Radio, _RadioInstance as RadioInstance };

declare global {
  namespace GUni {
    /**
     * 标识
     *
     * 被选中时，radio-group 的 change 事件会携带该 value
     */
    export type RadioValue = _RadioValue;
    /** 单选项目属性 */
    export type RadioProps = _RadioProps;
    /** 单选项目 */
    export type Radio = _Radio;
    /** 单选项目实例 */
    export type RadioInstance = _RadioInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /** 单选项目 */
    Radio: _Radio;
  }
}
