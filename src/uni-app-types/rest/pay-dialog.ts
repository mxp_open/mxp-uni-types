/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../utils/component';
import type { CustomEvent } from '../events';

/** 解锁本集需要的消耗，当配置了此参数时需要配置 accountInfo */
type _PayDialogUnlockPrice = {
  /** 数量 */
  count: number;
  /** 单位 */
  unit: string;
};

/** 账户剩余权益 ，当配置了此参数时需要配置 unlockPrice */
type _PayDialogAccountInfo = {
  /** 数量 */
  count: number;
  /** 单位 */
  unit: string;
};
/** 广告配置信息 */
type _PayDialogAdConfig = {
  /** 广告类型 */
  type: number;
  /** 广告单元 id */
  unitId: number;
};

/** 标签 tag */
type _PayDialogSkuLabel = {
  /** 商品库配置的商品 id */
  skuId: string;
  /** tag的名称 */
  value: number | string | null | undefined;
};

/** 支付模板关闭事件的详情 */
interface _PayDialogOnCloseDetail {
  [x: string]: any;
  /**
   * 关闭弹窗的类型
   * 0	用户主动点击关闭权益弹窗 icon
   * 1	系统支付完成自动关闭
   * 2	广告解锁完成自动关闭
   * 3	仅配置了广告，当用户点击关闭广告按钮
   * 4	弹窗未正常拉起
   */
  code: number;
}

/** 支支付模板关闭的event对象 */
type _PayDialogOnCloseEvent = CustomEvent<_PayDialogOnCloseDetail>;
/** 支付模板关闭时触发 */
interface _PayDialogOnClose {
  (event: _PayDialogOnCloseEvent): void;
}

/** 支付错误事件的详情 */
interface _PayDialogOnErrorDetail {
  [x: string]: any;
  /**
   *组件内部错误信息，如传入属性类型错误等
   */
  errMsg: number;
  /**
   * 错误码，对应某种具体报错原因
   */
  errCode: number;
}
/** 支付错误事件的event对象 */
type _PayDialogOnErrorEvent = CustomEvent<_PayDialogOnErrorDetail>;
/** 支付错误时触发 */
interface _PayDialogOnError {
  (event: _PayDialogOnErrorEvent): void;
}

/** 支付成功的信息 */
type _PayDialogPaySuccess = {
  /** 支付状态 0：支付成功 */
  code: number;
  /** 快手交易系统内部交易订单号 */
  orderId: string;
};

/** 支付失败的信息 */
type _PayDialogPayFail = {
  /**
   * API 支付错误码对应的信息
   * 105：支付取消
   * 106：支付超时
   * 107：请求错误
   * 108：网络异常
   * 203：服务端内部错误
   */
  errMsg: number;
  /**
   * 支付错误码
   * 105：支付取消
   * 106：支付超时
   * 107：请求错误
   * 108：网络异常
   * 203：服务端内部错误
   */
  errCode: string;
  /** 快手交易系统内部交易订单号 */
  orderId: string;
};
/** 支付回调的event对象的详情 */
type _PayDialogOnPayDetail =
  | {
      /**
       * 是否成功拉起收银台（微信/支付/IAP）
       * 'success': 成功
       * 'fail': 失败
       */
      status: 'fail';
      /** 快手交易系统内部订单号 */
      orderId: string;
      /** 开发者系统交易订单号 */
      outOrderNo: string;
      /** 根据 status 属性返回支付结果 */
      result: _PayDialogPayFail;
    }
  | {
      /**
       * 是否成功拉起收银台（微信/支付/IAP）
       * 'success': 成功
       * 'fail': 失败
       */
      status: 'success';
      /** 快手交易系统内部订单号 */
      orderId: string;
      /** 开发者系统交易订单号 */
      outOrderNo: string;
      /** 根据 status 属性返回支付结果 */
      result: _PayDialogPaySuccess;
    };

/** 支付回调的event对象 */
type _PayDialogOnPayEvent = CustomEvent<_PayDialogOnPayDetail>;
/** 支付完成时触发 */
interface _PayDialogOnPay {
  (event: _PayDialogOnPayEvent): void;
}

/** 广告解锁成功的信息 */
type _PayDialogAdSuccess = {
  /**
   * 0：视频播放完以后关闭的视频
   * 1：视频播放过程中关闭了视频
   * 开发者需要根据 result.code 判断视频是否播放结束、才可以向用户下发奖励。
   */
  code: number;
};

/** 广告解锁失败的信息 */
type _PayDialogAdFail = {
  /**
   * API 广告错误码对应的信息
   * 0    内部错误
   * 1    广告对象已关闭
   * 2    广告数据暂未准备好
   * 3    网络错误
   * 4    广告校验失败
   * 5    详情页正在展示
   * 1006 过早展示广告，需在规定时间后再展示广告
   */
  errMsg: number;
  /**
   * API 广告错误码
   * 0    内部错误
   * 1    广告对象已关闭
   * 2    广告数据暂未准备好
   * 3    网络错误
   * 4    广告校验失败
   * 5    详情页正在展示
   * 1006 过早展示广告，需在规定时间后再展示广告
   */
  errCode: number;
};

/** 广告解锁回调的event对象的详情 */
type _PayDialogOnAdDetail =
  | {
      /**
       * 是否成功播放广告
       * 'success': 成功
       * 'fail': 失败
       */
      status: 'fail';
      /** 根据 status 属性返回广告结果  */
      result: _PayDialogAdFail;
    }
  | {
      /**
       * 是否成功播放广告
       * 'success': 成功
       * 'fail': 失败
       */
      status: 'success';
      /** 根据 status 属性返回广告结果  */
      result: _PayDialogAdSuccess;
    };
/** 广告解锁回调的event对象 */
type _PayDialogOnAdEvent = CustomEvent<_PayDialogOnAdDetail>;

/** 广告解锁回调时触发 */
interface _PayDialogOnAd {
  (event: _PayDialogOnAdEvent): void;
}

/** 视频播放组件属性 */
type _PayDialogProps = Partial<{
  /** 商品库配置的商品 id 数组，0~6，配置 0 个表示仅有广告解锁 */
  skuIdList: string[];
  /** 解锁本集需要的消耗，当配置了此参数时需要配置 accountInfo */
  unlockPrice: _PayDialogUnlockPrice;
  /** 账户剩余权益数组 ，当配置了此参数时需要配置 unlockPrice */
  accountInfo: _PayDialogAccountInfo[];
  /**	内容来源 */
  origin: string;
  /**	广告配置信息 */
  adConfig: _PayDialogAdConfig;
  /**	副标题 */
  subtitle: string;
  /**	标签 tag 列表 */
  skuLabels: _PayDialogSkuLabel[];
  /** 选中的商品 id，默认选中第一个 */
  selectedSkuId: string;
  /** 弹窗的层级 */
  zIndex: number;
  /** 开发者自定义字段，回调原样回传  */
  attach: string;
  /** 关闭弹窗事件回调 (解决 uni事件监听失效的问题) */
  onClose: _PayDialogOnClose;
  /** 支付回调 (解决 uni事件监听失效的问题) */
  onPay: _PayDialogOnPay;
  /** 广告解锁回调 (解决 uni事件监听失效的问题) */
  onAd: _PayDialogOnAd;
  /** 组件内部错误信息回调 (解决 uni事件监听失效的问题) */
  onError: _PayDialogOnError;
}>;

/** 视频播放组件 */
type _PayDialog = Component<_PayDialogProps>;

/** 视频播放组件实例 */
type _PayDialogInstance = InstanceType<_PayDialog>;

export type {
  _PayDialogUnlockPrice as PayDialogUnlockPrice,
  _PayDialogAccountInfo as PayDialogAccountInfo,
  _PayDialogAdConfig as PayDialogAdConfig,
  _PayDialogSkuLabel as PayDialogSkuLabel,
  _PayDialogOnCloseDetail as PayDialogOnCloseDetail,
  _PayDialogOnCloseEvent as PayDialogOnCloseEvent,
  _PayDialogOnClose as PayDialogOnClose,
  _PayDialogOnErrorDetail as PayDialogOnErrorDetail,
  _PayDialogOnErrorEvent as PayDialogOnErrorEvent,
  _PayDialogOnError as PayDialogOnError,
  _PayDialogPaySuccess as PayDialogPaySuccess,
  _PayDialogPayFail as PayDialogPayFail,
  _PayDialogOnPayDetail as PayDialogOnPayDetail,
  _PayDialogOnPayEvent as PayDialogOnPayEvent,
  _PayDialogOnPay as PayDialogOnPay,
  _PayDialogAdSuccess as PayDialogAdSuccess,
  _PayDialogAdFail as PayDialogAdFail,
  _PayDialogOnAdDetail as PayDialogOnAdDetail,
  _PayDialogOnAdEvent as PayDialogOnAdEvent,
  _PayDialogOnAd as PayDialogOnAd,
  _PayDialogProps as PayDialogProps,
  _PayDialog as PayDialog,
  _PayDialogInstance as PayDialogInstance,
};

declare global {
  namespace GUni {
    /** 解锁本集需要的消耗，当配置了此参数时需要配置 accountInfo */
    export type PayDialogUnlockPrice = _PayDialogUnlockPrice;
    /** 账户剩余权益 ，当配置了此参数时需要配置 unlockPrice */
    export type PayDialogAccountInfo = _PayDialogAccountInfo;
    /** 广告配置信息 */
    export type PayDialogAdConfig = _PayDialogAdConfig;
    /** 标签 tag */
    export type PayDialogSkuLabel = _PayDialogSkuLabel;
    /** 支付模板关闭事件的详情 */
    export type PayDialogOnCloseDetail = _PayDialogOnCloseDetail;
    /** 支支付模板关闭的event对象 */
    export type PayDialogOnCloseEvent = _PayDialogOnCloseEvent;
    /** 支付模板关闭时触发 */
    export type PayDialogOnClose = _PayDialogOnClose;
    /** 支付错误事件的详情 */
    export type PayDialogOnErrorDetail = _PayDialogOnErrorDetail;
    /** 支付错误事件的event对象 */
    export type PayDialogOnErrorEvent = _PayDialogOnErrorEvent;
    /** 支付错误时触发 */
    export type PayDialogOnError = _PayDialogOnError;
    /** 支付成功的信息 */
    export type PayDialogPaySuccess = _PayDialogPaySuccess;
    /** 支付失败的信息 */
    export type PayDialogPayFail = _PayDialogPayFail;
    /** 支付回调的event对象的详情 */
    export type PayDialogOnPayDetail = _PayDialogOnPayDetail;
    /** 支付回调的event对象 */
    export type PayDialogOnPayEvent = _PayDialogOnPayEvent;
    /** 支付完成时触发 */
    export type PayDialogOnPay = _PayDialogOnPay;
    /** 广告解锁成功的信息 */
    export type PayDialogAdSuccess = _PayDialogAdSuccess;
    /** 广告解锁失败的信息 */
    export type PayDialogAdFail = _PayDialogAdFail;
    /** 广告解锁回调的event对象的详情 */
    export type PayDialogOnAdDetail = _PayDialogOnAdDetail;
    /** 广告解锁回调的event对象 */
    export type PayDialogOnAdEvent = _PayDialogOnAdEvent;
    /** 广告解锁回调时触发 */
    export type PayDialogOnAd = _PayDialogOnAd;
    /** 视频播放组件属性 */
    export type PayDialogProps = _PayDialogProps;
    /** 视频播放组件 */
    export type PayDialog = _PayDialog;
    /** 视频播放组件实例 */
    export type PayDialogInstance = _PayDialogInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * 快手专用支付模板
     */
    PayDialog: _PayDialog;
  }
}
