/* eslint-disable @typescript-eslint/no-namespace */
import type { AnyRecord, Component } from '../../utils/component';

/** 视频播放组件属性 */
type _BlockProps = Partial<AnyRecord>;

/** 视频播放组件 */
type _Block = Component<_BlockProps>;

/** 视频播放组件实例 */
type _BlockInstance = InstanceType<_Block>;

export type { _BlockProps as BlockProps, _Block as Block, _BlockInstance as BlockInstance };

declare global {
  namespace GUni {
    /**
     * 类似template
     */
    export type Block = _Block;
    /** Block组件实例 */
    export type BlockInstance = _BlockInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    /**
     * 类似template
     */
    Block: _Block;
  }
}
