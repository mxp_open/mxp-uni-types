/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../../utils/component';
import type { MescrollEmits, MescrollProps } from '../utils/public';
/** Mescroll组件 */
type _MescrollUni = Component<MescrollEmits & MescrollProps>;

/** Mescroll组件实例  */
type _MescrollUniInstance = InstanceType<_MescrollUni>;

export type { MescrollProps as MescrollUniProps, _MescrollUni as MescrollUni, _MescrollUniInstance as MescrollUniInstance };

declare global {
  namespace GUni {
    export type MescrollUniProps = MescrollProps;
    export type MescrollUni = _MescrollUni;
    export type MescrollUniInstance = _MescrollUniInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    MescrollUni: _MescrollUni;
  }
}
