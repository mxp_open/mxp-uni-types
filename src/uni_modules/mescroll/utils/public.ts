/* eslint-disable no-use-before-define */

/* eslint-disable @typescript-eslint/no-namespace */
// mescroll 文档  https://www.mescroll.com/uni.html#options

/** 回到顶部按钮的配置 */
type _MescrollToTop = {
  /** 图片路径,必须配置src才会显示; (若想禁用,则src为空串即可) */
  src?: number | string;
  /** 列表滚动1000px显示回到顶部按钮 */
  offset?: number | string;
  /** 回到顶部的动画时长, 默认300ms (当值为0或300则使用系统自带回到顶部,更流畅; 其他值则通过js模拟,比较耗性能; 所以非特殊情况不建议修改) */
  duration?: number | string;
  /** fixed定位z-index值 (1.2.0新增) */
  zIndex?: number | string;
  /** 到右边的距离 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增) */
  right?: number | string;
  /** 到底部的距离 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增) */
  bottom?: number | string;
  /** bottom的偏移量是否加上底部安全区的距离, 默认false. 适配iPhoneX时使用 (具体页面如不配此项,则取mescroll组件props的safearea值. 1.2.0新增) */
  safearea?: number | string;
  /** 回到顶部图标的宽度 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增) */
  width?: number | string;
  /** 圆角 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增) */
  radius?: number | string;
  /** 到左边的距离. 此项有值时,right不生效. (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增) */
  left?: number | string | null;
};
/** 列表第一页无任何数据时,显示的空布局 */
type _MescrollEmpty = {
  /** 是否启用 */
  use?: boolean;
  /** 空布局的图标路径 (支持网络路径) */
  icon?: string;
  /** 提示文本 */
  tip?: string;
  /** 按钮文本 */
  btnText?: string;
  /** 是否使用定位,默认false; 配置fixed为true,以下的top和zIndex才生效 (transform会使fixed失效,所以最终还是会降级为absolute) */
  fixed?: false;
  /** fixed定位的top值 (完整的单位值,如"100rpx", "10%";) */
  top?: string;
  /** fixed定位z-index值 */
  zIndex?: number;
};

/** 上拉加载的页码配置 */
type _MescrollPage = {
  /** 当前页码,默认0,回调之前加1,即callback(page)从1开始; */
  num: number;
  /** 每页数据的数量; 默认10 */
  size: number;
  /** 加载第一页数据服务器返回的时间 (可空); 防止用户翻页时,后台新增了数据从而导致下一页数据重复; */
  time?: number | string;
};

type _MescrollDownText = {
  /** 下拉的距离在offset范围内的提示文本 */
  textInOffset?: string;
  /** 下拉的距离大于offset范围的提示文本 */
  textOutOffset?: string;
  /** 加载中的提示文本 */
  textLoading?: string;
  /** 加载成功的提示文本 (1.3.3新增) */
  textSuccess?: string;
  /** 加载失败的提示文本 (1.3.3新增) */
  textErr?: string;
};

/** 下拉刷新的配置参数 (可通过 mescroll.optDown 动态调整) */
type _MescrollDown = _MescrollDownText & {
  /** 是否启用下拉刷新 如果配置false,则不会初始化下拉刷新的布局 */
  use?: boolean;
  /** 是否在初始化完毕之后自动执行一次下拉刷新的回调 callback */
  auto?: boolean;
  /**
   * 是否使用系统自带的下拉刷新; 默认false; 仅mescroll-body生效
   * 值为true时,需在pages配置"enablePullDownRefresh":true 和 "mp-alipay":{"allowsBounceVertical":"YES"}
   * 详请参考mescroll-native的案例 (1.2.1版本新增)
   */
  native?: boolean;

  /** 延时结束的时长, 也是显示加载成功/失败的时长; 单位ms (1.3.3新增) */
  beforeEndDelay?: number;
  /**
   * 下拉区域背景颜色 (1.2.4新增)
   * 支持背景图和渐变: 如 'url(xxx) 0 0/100% 100%', 'linear-gradient(xx)' (1.2.6版本)
   * 支持一行代码定义background: https://www.runoob.com/cssref/css3-pr-background.html
   * 建议同步配置pages.json的"backgroundColorTop"
   */
  bgColor?: string;
  /** 下拉文本的颜色 (当bgColor配置了颜色,textColor未配置时,则会默认为白色 1.2.4新增) */
  textColor?: string;
  /**
   * 如果设置auto=true ( 在初始化完毕之后自动执行下拉刷新的回调 ) ,
   * 那么是否显示下拉刷新的进度
   * 需配置down的callback才生效
   */
  autoShowLoading?: boolean;
  /** 如果配置true,则会锁定不可下拉,可通过调用mescroll.lockDownScroll(false)解锁 */
  isLock?: boolean;
  /** 在列表顶部,下拉大于80px,松手即可触发下拉刷新的回调 */
  offset?: number;
  /**
   * 下拉的节流配置 (值越大每秒更新下拉状态的频率越高, 当您发觉下拉卡顿时可尝试调高/低此值)
   * 1.3.0版本已废弃, 已通过wxs提高性能, 无需手动节流
   */
  fps?: number;
  /** 在列表顶部,下拉的距离小于offset时,改变下拉区域高度比例;值小于1且越接近0,高度变化越小,表现为越往下越难拉 */
  inOffsetRate?: number;
  /**
   * 在列表顶部,下拉的距离大于offset时,改变下拉区域高度比例;
   * 值越接近0,高度变化越小,表现为越往下越难拉
   */
  outOffsetRate?: number;
  /** 下拉区域的高度 (1.2.4新增) */
  bottomOffset?: number;
  /**
   * 触发下拉最少要偏移的角度(滑动的轨迹与水平线的锐角值),取值区间 [0,90];
   * 默认45度,即向下滑动的角度大于45度(方位角为45°~145°及225°~315°)则触发下拉;
   * 而小于45度,将不触发下拉,避免与左右滑动的轮播等组件冲突
   */
  minAngle?: number;
};

type _MescrollUpText = {
  /** 上拉加载中的文本 */
  textLoading?: number;
  /** 没有更多数据的提示文本 */
  textNoMore?: number;
};

/** 上拉加载的配置参数 (可通过 mescroll.optUp 动态调整) */
type _MescrollUp = _MescrollUpText & {
  /** 是否启用上拉加载 如果配置false,则不会初始化上拉加载的布局  */
  use?: boolean;
  /**
   * 是否在初始化完毕之后自动执行一次上拉加载的回调
   * 当配置为false时,建议down的auto也为false,因为downCallback默认调用resetUpScroll,最终还会触发upCallback
   * 如果是想实现返回刷新页面,那么其实不需要设置auto为false,详情请看：https://www.mescroll.com/uni.html?v=1.3.8#note
   */
  auto?: boolean;
  /**
   * 是否允许橡皮筋回弹效果, 默认不允许; 1.1.5新增
   * 1.3.0版本已废弃, 已通过renderjs自动判断, 无需配置mescroll-touch
   */
  isBounce?: number;
  /**
   * 页码信息：示例  { num : 0 , size : 10 , time : null }
   * num代表 : 当前页码,默认0,回调之前加1,即callback(page)从1开始;
   * size代表 : 每页数据的数量; 默认10
   * time代表 : 加载第一页数据服务器返回的时间 (可空); 防止用户翻页时,后台新增了数据从而导致下一页数据重复;
   */
  page?: _MescrollPage;
  /**
   * 如果列表已无数据,可设置列表的总数量要大于5条才显示无更多数据;
   * 避免列表数据过少(比如只有一条数据),显示无更多数据会不好看
   * 这就是为什么无更多数据 有时候不显示的原因了
   */
  noMoreSize?: number;

  /** 上拉区域背景颜色 (1.2.4新增) */
  bgColor?: string;
  /**
   * 上拉文本的颜色 (当bgColor配置了颜色,textColor未配置时,则会默认为白色 1.2.4新增)
   * 支持背景图和渐变: 如 'url(xxx) 0 0/100% 100%', 'linear-gradient(xx)' (1.2.6版本)
   * 支持一行代码定义background: https://www.runoob.com/cssref/css3-pr-background.html
   * 建议同步配置pages.json的"backgroundColorBottom"
   */
  textColor?: number;
  /**
   * 回到顶部按钮的配置:{ src: null, offset:1000, duration :300, zIndex: 9990, right: 20,  bottom: 120, safearea: false, width: 72, radius: "50%", left: null }
   * src : 图片路径,必须配置src才会显示; (若想禁用,则src为空串即可)
   * offset : 列表滚动1000px显示回到顶部按钮
   * duration : 回到顶部的动画时长, 默认300ms (当值为0或300则使用系统自带回到顶部,更流畅; 其他值则通过js模拟,比较耗性能; 所以非特殊情况不建议修改)
   * zIndex : fixed定位z-index值 (1.2.0新增)
   * right: 到右边的距离 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增)
   * bottom: 到底部的距离 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增)
   * safearea: bottom的偏移量是否加上底部安全区的距离, 默认false. 适配iPhoneX时使用 (具体页面如不配此项,则取mescroll组件props的safearea值. 1.2.0新增)
   * width: 回到顶部图标的宽度 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增)
   * radius: 圆角 (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增)
   * left: 到左边的距离. 此项有值时,right不生效. (支持"20rpx", "20px", "20%"格式的值, 纯数字则默认单位rpx. 1.2.0新增)
   */
  toTop?: _MescrollToTop;
  /**
   * 列表第一页无任何数据时,显示的空布局
   * use : 是否启用
   * icon : 空布局的图标路径 (支持网络路径)
   * tip : 提示文本
   * btnText : 按钮文本
   * fixed : 是否使用定位,默认false; 配置fixed为true,以下的top和zIndex才生效 (transform会使fixed失效,所以最终还是会降级为absolute)
   * top : fixed定位的top值 (完整的单位值,如"100rpx", "10%";)
   * zIndex : fixed定位z-index值
   */
  empty?: _MescrollEmpty;
  /** 上拉加载时,如果滑动到列表顶部是否可以同时触发下拉刷新 */
  isBoth?: boolean;
  /** 是否锁定上拉加载  如果配置true,则会锁定不可上拉,可通过调用mescroll.lockUpScroll(false)解锁  */
  isLock?: boolean;
  /** 距底部多远时,触发upCallback ; 1.1.0新增 (仅mescroll-uni生效); mescroll-body配置的是pages.json的 onReachBottomDistance */
  offset?: number;
  /** 是否监听滚动事件, 默认false (仅mescroll-uni可用;mescroll-body是页面的onPageScroll); 监听列表滚动是非常耗性能的,很容易出现卡顿,非特殊情况不要配置此项 */
  onScroll?: boolean;
};

/** 下拉刷新的回调 (参数返回mescroll实例对象) */
interface _MescrollOnDown {
  (mescroll: _Mescroll): void;
}

/** 下拉刷新的回调 (参数返回mescroll实例对象) */
interface _MescrollOnUp {
  (mescroll: _Mescroll): void;
}
/** 下拉刷新的回调 (参数返回mescroll实例对象) */
interface _MescrollOnInit {
  (mescroll: _Mescroll): void;
}
/** 点击empty配置的btnText按钮回调 (返回mescroll实例对象) */
interface _MescrollOnEmptyclick {
  (mescroll: _Mescroll): void;
}
/** 点击回到顶部的按钮回调 (返回mescroll实例对象) */
interface _MescrollOnTopclick {
  (mescroll: _Mescroll): void;
}
/**
 * 滚动监听 (需在up配置onScroll:true;仅mescroll-uni可用;mescroll-body是页面的onPageScroll)
 * console.log('当前滚动条的位置:' + mescroll.scrollTop + ', 是否向上滑:'+mescroll.isScrollUp)
 */
interface _MescrollOnScroll {
  (mescroll: _Mescroll): void;
}

type _MescrollI18n = Record<
  string,
  {
    down: _MescrollDownText;
    up: _MescrollUpText & {
      empty: {
        /** 空状态的提示文本 */
        tip: string;
      };
    };
  }
> & {
  type: string;
};

/** emit */
type _MescrollEmits = Partial<{
  /** 点击empty配置的btnText按钮回调 (返回mescroll实例对象) */
  onEmptyclick: _MescrollOnEmptyclick;
  /** 点击回到顶部的按钮回调 (返回mescroll实例对象) */
  onTopclick: _MescrollOnTopclick;
  /**
   * 滚动监听 (需在up配置onScroll:true;仅mescroll-uni可用;mescroll-body是页面的onPageScroll)
   * console.log('当前滚动条的位置:' + mescroll.scrollTop + ', 是否向上滑:'+mescroll.isScrollUp)
   */
  onScroll: _MescrollOnScroll;
  /** 下拉刷新的回调 (参数返回mescroll实例对象) */
  onDown: _MescrollOnDown;
  /**
   * 上拉加载的回调 (返回mescroll实例对象)
   * 此时mescroll会携带page的参数
   * 其中mescroll.num:当前页 从1开始, mescroll.size:每页数据条数,默认10
   */
  onUp: _MescrollOnUp;
  /** mescroll组件初始化完成的回调 (返回mescroll实例对象) */
  onInit: _MescrollOnInit;
}>;

type _MescrollOp = {
  /** 下拉刷新的配置参数 (可通过 mescroll.optDown 动态调整)  */
  down: _MescrollDown;
  /** 上拉加载的配置参数 (可通过 mescroll.optUp 动态调整) */
  up: _MescrollUp;
  /**
   * 是否通过fixed定位来固定mescroll-uni的高度,默认true; (mescroll-body不生效,因为无需固定高度)
   * 当配置:fixed="false"时,则mescroll-uni高度跟随父元素, 此时父元素必须有固定的高度,否则列表滚动异常
   * 所以不想使用fixed, 建议通过flex固定高度 或 配置height来固定mescroll-uni的高度,
   */
  fixed: boolean;
  /**
   * 对mescroll-uni可简单快捷设置的高度, 此项有值,则不使用fixed (1.2.0新增)
   * 对mescroll-body可设置最小高度,默认100%,使列表不满屏仍可下拉 (1.2.1新增)
   * 支持100, "100rpx", "100px", "100%"格式的值
   * 其中纯数字则默认单位rpx, 百分比则相对于windowHeight
   */
  height: number | string;
  /**
   * 下拉刷新区域往下偏移的距离
   * 比如希望偏移100rpx, 则top="100", 传的值是rpx的数值
   * 当:fixed="true", 此时top为fixed的top (已自动加上window-top的值)
   * 当:fixed="false"则为padding-top
   * 1.2.0版本开始支持"100rpx", "100px", "100%"格式的值
   */
  top: number | string;
  /**
   * top的偏移量是否加上状态栏高度 (mescroll-uni当fixed为false时不生效, mescroll-body生效) 1.1.9新增
   * 使用场景: 取消原生导航栏时,配置此项可自动加上状态栏高度的偏移量
   * 1.2.6版本: 支持传入字符串背景,如 色值'#ffff00', 背景图'url(xxx) 0 0/100% 100%', 渐变'linear-gradient(xx)'
   * 支持一行代码定义background: https://www.runoob.com/cssref/css3-pr-background.html
   */
  topbar: boolean | string;
  /**
   * 上拉加载区域往上偏移的距离
   * 比如希望偏移100rpx, 则bottom="100", 传的值是rpx的数值
   * 当:fixed="true", 此时bottom为fixed的bottom (已自动加上window-bottom的值)
   * 当:fixed="false"则为padding-bottom
   * 1.2.0版本开始支持"100rpx", "100px", "100%"格式的值
   */
  bottom: number | string;
  /** tab页是否偏移TabBar的高度,避免列表被TabBar遮住, 默认true (仅h5生效,仅h5需要, 1.2.7新增) */
  bottombar: boolean;
  /** bottom的偏移量是否加上底部安全区的距离 (1.2.0新增) 适配iPhoneX时使用, 此项值对回到顶部按钮生效 */
  safearea: boolean;
  /** 国际化语言配置, 参考 mescroll-i18n.vue 的示例 超简单 */
  i18n: _MescrollI18n;
  /**
   * 错误或者空状态的容器高度
   */
  emptyHeight: number | string;
  /** 吸顶,仅mescroll-body支持 */
  sticky: boolean;
  /** 是否禁止滚动,仅mescroll-uni支持  */
  disableScroll: boolean;
};

type _MescrollOnScrollEvent = {
  /** 滚动条的位置 */
  scrollTop: number;
  /** 滚动方向, true:向上滑, false:向下滑 */
  isScrollUp: boolean;
};

/** 属性 */
type _MescrollProps = Partial<_MescrollOp>;
/** Mescroll的实例注意不是组件或的实例，组件或的组件实例是MescrollBody,MescrollBodyInstance,MescrollUni,MescrolluniInstance  */
type _Mescroll = _MescrollOnScrollEvent &
  _MescrollPage &
  Partial<_MescrollOnScrollEvent> & {
    /** mescroll的版本号 */
    version: string;
    //  options?: _MescrollOp['up'] & _MescrollProps['down'];
    // isScrollBody: boolean;
    // isDownScrolling: boolean;
    // isUpScrolling: boolean;
    // isUpAutoLoad: boolean;
    // downHight?: number;
    // bodyHeight?: number;
    /** 可手动修改的上拉加载的配置参数 */
    optUp: _MescrollOp['up'];
    /** 可手动修改的下拉刷新的配置参数 */
    optDown: _MescrollOp['down'];
    /** 起始数 */
    // startNum: number;
    /** 获取mescroll的唯一元素id 配合uni.createSelectorQuery获取更多信息, mescroll-uni支持,mescroll-body不支持 */
    viewId: string;
    /** 获取滚动的方向 (true向上滑 / false向下滑, mescroll-uni支持, mescroll-body不支持) */
    isScrollUp?: boolean;

    /**
     * 隐藏下拉刷新和上拉加载的状态, 在联网获取数据成功后调用
     * @param dataSize - 当前页获取的数据总数(注意是当前页)
     * @param totalPage - 列表的总页数
     * @param systime - systime : 加载第一页数据的服务器时间 (点击查看详情);
     */
    endByPage: (dataSize?: number, totalPage?: number, systime?: number | string) => void;
    /**
     * 隐藏下拉刷新和上拉加载的状态, 在联网获取数据成功后调用
     * @param dataSize - 当前页获取的数据总数(注意是当前页)
     * @param totalSize - 列表的总数据量
     * @param systime - 加载第一页数据的服务器时间 (可空);
     */
    endBySize: (dataSize?: number, totalSize?: number, systime?: number | string) => void;

    /**
     * 隐藏下拉刷新和上拉加载的状态, 在联网获取数据成功后调用
     * @param dataSize - 当前页获取的数据总数(注意是当前页)
     * @param hasNext - 是否有下一页数据true/false
     * @param systime - 加载第一页数据的服务器时间 (可空);
     */
    endSuccess: (dataSize?: number, hasNext?: number, systime?: number | string) => void;

    /**
     * 隐藏下拉刷新和上拉加载的状态, 在联网获取数据失败后调用;
     * mescroll内部会自动恢复原来的页码,时间等变量
     */
    endErr: () => void;

    /**
     * 重置列表为第一页 (常用于列表筛选条件变化或切换菜单时重新刷新列表数据) , 内部实现: 把page.num=1,再主动触发up.callback
     * @param hasNext - isShowLoading 是否显示进度布局;
     * 1.默认null,不传参,则显示上拉加载的进度布局
     * 2.传参true, 则显示下拉刷新的进度布局
     * 3.传参false,则不显示上拉和下拉的进度 (常用于静默更
     */
    resetUpScroll: (isShowLoading?: boolean | null) => void;

    /**
     * 主动触发下拉刷新
     */
    triggerDownScroll: () => void;

    /**
     * 主动触发上拉加载
     */
    triggerUpScroll: () => void;

    /**
     * 设置当前page.num的值; 如果要重置列表数据,请使用 mescroll.resetUpScroll(), 而不是setPageNum(1)
     * @param num - 需要设置的页码
     */
    setPageNum: (num: number) => void;
    /**
     * 设置当前page.size的值
     * @param size - 需要设置的每一页加载多少条数据
     */
    setPageSize: (size: number) => void;
    /**
     * 锁定下拉刷新
     * @param isLock - isLock=true,null 锁定 ; isLock=false 解锁
     */
    lockDownScroll: (isLock?: boolean | number) => void;
    /**
     * 锁定上拉加载
     * @param isLock - isLock=true,null 锁定 ; isLock=false 解锁
     */
    lockUpScroll: (isLock?: boolean | number) => void;

    /**
     * 滚动列表到指定位置
     * @param y - 为 0则回到列表顶部; 如需滚动到列表底部,可设置y很大的值,比如y=99999,当y为css选择器时,则mescroll可滚动到指定view (需更新到1.3.2)
     * @param t - 滚动动画的时长,单位ms,默认300; 如果不需要动画缓冲效果,则传0
     */
    scrollTo: (y: number | string, t: number) => void;

    /**
     * 显示下拉刷新的进度布局
     */
    showDownScroll: () => void;

    /**
     * 隐藏下拉刷新的进度布局
     */
    endDownScroll: () => void;

    /**
     * 结束上拉加载的状态
     * @param isShowNoMore - true:显示无更多数据; false:隐藏上拉加载; null:保持当前状态;
     */
    endUpScroll: (isShowNoMore?: boolean | null) => void;

    /**
     * 显示上拉加载的进度布局
     */
    showUpScroll: () => void;

    /**
     * 显示上拉无更多数据的布局
     */
    showNoMore: () => void;
    /**
     * 隐藏上拉加载的布局
     */
    hideUpScroll: () => void;

    /**
     * 显示无任何数据的空布局
     */
    showEmpty: () => void;
    /**
     * 移除无任何数据的空布局
     */
    removeEmpty: () => void;
    /**
     * 隐藏回到顶部的按钮
     */
    hideTopBtn: () => void;
    /**
     * 获取滚动条的位置y; 也可以在up配置onScroll监听滚动条的位置
     */
    getScrollTop: () => number;
    /**
     * 获取body的高度
     */
    getBodyHeight: () => number;
    /**
     * 获取滚动内容的高度 (mescroll-uni支持, mescroll-body不支持)
     */
    getScrollHeight: () => number;
    /**
     * 获取mescroll的高度 ()
     */
    getClientHeight: () => number;
    /**
     * 获取到底部的距离 (mescroll-uni支持, mescroll-body不支持)
     */
    getScrollBottom: () => number;

    /**
     * 隐藏下拉刷新和上拉加载的状态，且根据 list 判断是否显示空布局，用于上拉加载和下拉刷新都使用的场景
     * 注意此方法为[mxp131011@qq.com]实现的方法，原理是在绑定 init 方法中添加给返回的 mescroll 添加本方法，所以一定要记得绑定 init
     * @param dataSize - 当前页获取的数据总数(注意是当前页)
     * @param totalSize - 列表的总数据量
     */
    customEndByListSuccess: (dataSize: number, totalSize: number) => void;
    /**
     * 隐藏下拉刷新和上拉加载的状态，且隐藏空布局或错误布局
     * 注意此方法为[mxp131011@qq.com]实现的方法，原理是在绑定 init 方法中添加给返回的 mescroll 添加本方法，所以一定要记得绑定 init
     */
    customEndSuccess: () => void;

    /**
     * 显示错误布局
     * 注意此方法为[mxp131011@qq.com]实现的方法，原理是在绑定 init 方法中添加给返回的 mescroll 添加本方法，所以一定要记得绑定 init
     * @param text - 错误提示语
     */
    customEndErr: (text: string) => void;
  };

declare global {
  namespace GUni {
    export type MescrollToTop = _MescrollToTop;
    export type MescrollEmpty = _MescrollEmpty;
    export type MescrollI18n = _MescrollI18n;
    export type MescrollPage = _MescrollPage;
    export type MescrollDown = _MescrollDown;
    export type MescrollUp = _MescrollUp;
    export type MescrollOnDown = _MescrollOnDown;
    export type MescrollOnUp = _MescrollOnUp;
    export type MescrollOnInit = _MescrollOnInit;
    export type MescrollOnEmptyclick = _MescrollOnEmptyclick;
    export type MescrollOnTopclick = _MescrollOnTopclick;
    export type MescrollOnScroll = _MescrollOnScroll;
    export type MescrollProps = _MescrollProps;
    export type Mescroll = _Mescroll;
  }
}

export type {
  _MescrollToTop as MescrollToTop,
  _MescrollEmpty as MescrollEmpty,
  _MescrollI18n as MescrollI18n,
  _MescrollPage as MescrollPage,
  _MescrollDown as MescrollDown,
  _MescrollUp as MescrollUp,
  _MescrollOnDown as MescrollOnDown,
  _MescrollOnUp as MescrollOnUp,
  _MescrollOnInit as MescrollOnInit,
  _MescrollOnEmptyclick as MescrollOnEmptyclick,
  _MescrollOnTopclick as MescrollOnTopclick,
  _MescrollOnScroll as MescrollOnScroll,
  _MescrollProps as MescrollProps,
  _MescrollEmits as MescrollEmits,
  _Mescroll as Mescroll,
};
