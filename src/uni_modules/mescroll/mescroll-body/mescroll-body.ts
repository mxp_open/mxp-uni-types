/* eslint-disable @typescript-eslint/no-namespace */
import type { Component } from '../../../utils/component';
import type { MescrollEmits, MescrollProps } from '../utils/public';
/** Mescroll组件 */
type _MescrollBody = Component<MescrollEmits & MescrollProps>;

/** Mescroll组件实例  */
type _MescrollBodyInstance = InstanceType<_MescrollBody>;

export type { MescrollProps as MescrollBodyProps, _MescrollBody as MescrollBody, _MescrollBodyInstance as MescrollBodyInstance };

declare global {
  namespace GUni {
    export type MescrollBodyProps = MescrollProps;
    export type MescrollBody = _MescrollBody;
    export type MescrollBodyInstance = _MescrollBodyInstance;
  }
}

declare module 'vue' {
  export interface GlobalComponents {
    MescrollBody: _MescrollBody;
  }
}
