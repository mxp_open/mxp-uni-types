/* eslint-disable @typescript-eslint/no-namespace */

import type { AllowedComponentProps, ComponentCustomProps, DefineComponent, ReservedProps, StyleValue } from 'vue';
import type { TouchEvent } from '../uni-app-types/events';
interface _OnClick {
  (event: TouchEvent): void;
}

interface _OnTouchstart {
  (event: TouchEvent): void;
}

interface _PublicProps extends ComponentCustomProps, Omit<AllowedComponentProps, 'class' | 'style'>, ReservedProps {
  /** 样式 */
  style?: StyleValue;
  /** class */
  class?: any;
  /**
   * id属性
   */
  id?: unknown;

  /**
   * 触摸开始事件
   */
  onTouchstart?: _OnTouchstart;

  /**
   * 触摸移动事件
   */
  onTouchmove?: _OnTouchstart;

  /**
   * 触摸结束事件
   */
  onTouchend?: _OnTouchstart;

  /**
   * 触摸取消事件
   */
  onTouchcancel?: _OnTouchstart;

  /**
   *长按事件
   */
  onLongpress?: _OnTouchstart;

  /**
   * 点击事件
   */
  onClick?: _OnClick;
}

type _AnyRecord = Record<string, any>;

type _Component<P extends _AnyRecord = _AnyRecord> = DefineComponent<P>;

/** 把驼峰转命名换为中横线命名 */
type _KebabCase<S extends string> = S extends `${infer First}${infer Rest}`
  ? Rest extends Uncapitalize<Rest>
    ? `${Lowercase<First>}${_KebabCase<Rest>}`
    : `${Lowercase<First>}-${_KebabCase<Rest>}`
  : S;

/** 把对象里的 key 驼峰转命名换为中横线命名 */
type _KebabCaseKeys<T extends Record<number | string | symbol, any>> = {
  [K in keyof T as _KebabCase<K & string>]: T[K];
};

declare global {
  namespace GUni {
    export type AnyRecord = _AnyRecord;
    export type Component = _Component;
    /** 把驼峰转命名换为中横线命名 */
    export type KebabCase<T extends string> = _KebabCase<T>;
    /** 把对象里的 key 驼峰转命名换为中横线命名 */
    export type KebabCaseKeys<T extends Record<number | string | symbol, any>> = _KebabCaseKeys<T>;
  }
}

export type { _AnyRecord as AnyRecord, _Component as Component, _PublicProps as PublicProps, _KebabCaseKeys as KebabCaseKeys, _KebabCase as KebabCase };
