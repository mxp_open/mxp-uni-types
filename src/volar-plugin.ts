import type { CompilerOptions } from '@vue/compiler-dom';

const nativeTags = ['block', 'component', 'template', 'slot'];

export default () => ({
  version: 2,
  resolveTemplateCompilerOptions(options: CompilerOptions): CompilerOptions {
    options.isNativeTag = (tag: string) => nativeTags.includes(tag);
    return options;
  },
});
