/* eslint-disable @typescript-eslint/no-namespace,no-use-before-define */
export {};
declare global {
  /** 引入微信短剧播放插件 */
  function requirePlugin(str: 'playlet-plugin'): WXPlaylet.PlayletPlugin;
  namespace WXPlaylet {
    export enum REPORT_DATA_EVENTS {
      /** 进入播放器页面 */
      LOAD = 'LOAD',
      /** 播放器页面show */
      SHOW = 'SHOW',
      /** 开始播放 */
      START_PLAY = 'START_PLAY',
      /** 离开页面或者退后台	例如{type: 1,likeAndFav:{}}。type为1表示Unload，为2表示hide，likeAndFav是object对象，表示用户在播放器内点赞、取消点赞、追剧、取消追剧的行为 */
      UNLOAD_OR_HIDE = 'UNLOAD_OR_HIDE',
      /** 播放视频结束 */
      VIDEO_END = 'VIDEO_END',
      /** 视频缓冲中，对应video的waiting事件 */
      VIDEO_START_WAITING = 'VIDEO_START_WAITING',
      /** 视频缓冲结束，stopTime表示VIDEO_START_WAITING到首个VIDEO_TIME_UPDATE的时间	例如{stopTime: 0}。stopTime表示缓冲了多久才继续播放，单位ms */
      VIDEO_WAITING = 'VIDEO_WAITING',
      /** 视频播放失败	例如{error: ''}。error表示错误信息 */
      VIDEO_ERROR = 'VIDEO_ERROR',
      /** 该剧已下架 */
      VIDEO_REMOVED = 'VIDEO_REMOVED',
      /** 切换剧集	例如{isFinish: true, slideType: 1}。isFinish为true表示是播放结束自动切换到下一集。slideType为1表示上一集播放结束自动切换，2表示手势滑动切换，3表示从选集弹窗切换 */
      CHANGE_SERIAL = 'CHANGE_SERIAL',
      /** 点击拉起切换剧集弹窗 */
      CLICK_CHANGE_SERIAL = 'CLICK_CHANGE_SERIAL',
      /** 点赞 */
      LIKE = 'LIKE',
      /** 取消点赞	例如{type: 2}。type为2表示双击屏幕点赞, 1表示点击icon点赞 */
      UNLIKE = 'UNLIKE',
      /** 分享	例如{shareType: 1, shareId: ''}。shareType为1表示分享好友，2表示分享海报。shareId表示每次分享唯一的字符串，会在分享的path上带出去 */
      SHARE = 'SHARE',
      /** 点击活动位 */
      CLICK_ACTIVITY = 'CLICK_ACTIVITY',
      /** 在追 */
      FAV = 'FAV',
      /** 取消在追 */
      UNFAV = 'UNFAV',
      /** 倍速播放	{source: 1, speedType: 1.5}。source为1表示点击倍速按钮切换，2表示长按视频切换到2倍速，speedType表示切换到的倍速，例如1.5倍速 */
      CHANGE_SPEED = 'CHANGE_SPEED',
      /** progress事件	参数参考video组件的progress事件 */
      VIDEO_PROGRESS = 'VIDEO_PROGRESS',
      /** timeupdate事件	参数参考video组件的timeupdate事件 */
      VIDEO_TIME_UPDATE = 'VIDEO_TIME_UPDATE',
      /** play事件 */
      VIDEO_PLAY = 'VIDEO_PLAY',
      /** 暂停事件 */
      VIDEO_PAUSE = 'VIDEO_PAUSE',
      /** 插件内部请求接口报错，ret表示错误码	{ret: 21003}，ret为1表示dramaId或srcAppid填错，21003表示解密失败 */
      REQUEST_ERROR = 'REQUEST_ERROR',
      /** 关注公众号 */
      ADD_FOLLOW_SUC = 'ADD_FOLLOW_SUC',
      /** 观看时长	{type: 1, playTime: 10, duration: 65}。type表示触发的时机，目前取值1表示unload，2表示hide，3表示某一集结束，4表示用户手动拖动进度条，5表示切换了剧集，playTime是此次观看时长 */
      WATCH_TIME = 'WATCH_TIME',
      /** 点击解锁按钮 */
      TAP_UNLOCK = 'TAP_UNLOCK',
    }
    /** 播放器页面实例的唯一ID 由于可能页面栈上会存在多个播放器页面，所以需要通过这个唯一的ID来区分不同的页面，例如从播放器的推荐跳转到另外一个剧。 */
    export type PlayerId = string;

    export interface FreeItem {
      /**起始免费剧集编号，从1开始  */
      start_serial_no: number;
      /** 结束免费剧集编号，包含此集 */
      end_serial_no: number;
    }

    export interface SetCanPlaySerialListParam {
      /** data是加密串具体参考短剧播放器加密方案章节 */
      data: string;
      /** 免费的集数 */
      freeList?: FreeItem[];
    }

    export interface SetFreeListParam {
      /** 免费的集数 */
      list?: FreeItem[];
    }

    export interface ParamInfo {
      /** 播放器页面唯一ID，可通过此参数获取到PlayletManager实例 */
      playerId: string;
      /** 提审方appid */
      srcAppid: string;
      /** 媒资管理后台上传的剧目id */
      dramaId: string;
      /** 当前播放到第几集 */
      serialNo: number;
      /** 扩展参数，如手动decodeURIComponent()编码，过需要用decodeURIComponent()解码  */
      extParam: string;
    }

    export interface OnHideChargeDialogEvent{
      /** 播放器页面唯一ID，可通过此参数获取到PlayletManager实例 */
      playerId?: string;
      /** 提审方appid */
      srcAppid?: string;
      /** 媒资管理后台上传的剧目id */
      dramaId?: string;
      /** 当前播放到第几集 */
      serialNo?: number;
      /** 扩展参数，如手动decodeURIComponent()编码，过需要用decodeURIComponent()解码  */
      extParam?: string;
    }


    export type OnCheckIsCanPlayEvent = {
      /** 媒资管理后台上传的剧目id */
      dramaId: string;
      /** 剧集ID，从 1 开始 */
      serialNo: number | string;
      /** 播放器页面唯一ID，可通过此参数获取到PlayletManager实例 */
      playerId: string;
      /** 提审方appid */
      srcAppid: string;
      /** 是否因重试而触发。当遇到可能由于 session_key 过期等原因引发解密失败时，插件会重试一次触发 onCheckIsCanPlay */
      isRetry?: boolean;
    };

    export type OnCheckIsCanPlay = (option: OnCheckIsCanPlayEvent) => void;

    export type IsCanPlayEvent = {
      /** 可播放剧集的加密串，具体的含义和setCanPlaySerialList一样,当传入 useLastStatus=true 时可不传, */
      data?: string;
      /** 剧集ID，从 1 开始 */
      serialNo: number;
      /** 表示维持之前设置的剧集上锁状态，此字段设置为 true 时可不传 data 字段 */
      useLastStatus?: boolean;
    };

    export interface ChangeDramaEvent {
      /** key=value&key1=value1格式的参数 */
      queryString: string;
    }

    export interface UpdateChargeDialogInfoEvent {
      /** 默认false，设置为true，弹窗背景为透明 */
      isTransparentBackground?: boolean;
      /** 默认false，设置为true表示每次收起充值弹窗后销毁充值组件 */
      isDestroyOnHide?: boolean;
    }

    export interface UpdateChargeDialogHeightEvent {
      /** 设置充值弹窗高度 单位px */
      height: number;
    }

    export interface SetExtParamOption {
      /**默认true，是否打开分享 */
      share: boolean;
      /** 是否使用带 shareTicket 的转发 默认true，同小程序分享的withShareTicket */
      withShareTicket?: boolean;
    }

    export interface LeftSideOption {
      /** 默认false，设置组件的距离左侧位置 */
      left?: number;
      /** 默认false，设置组件的距离顶部位置 */
      top?: number;
      /** 设置组件宽度 */
      width?: number;
      /** 设置组件高度 */
      height?: number;
    }

    export interface UpdateOpenAreaOption {
      /** 默认false，是否显示组件open-area-left */
      showLeft?: boolean;
      /** 默认false，是否显示组件open-area-right */
      showRight?: boolean;
      /** 设置组件open-area-left的宽度 */
      leftWidth?: number;
      /** 设置组件open-area-left的高度 */
      leftHeight?: number;
      /** 设置组件open-area-right的高度 */
      rightHeight?: number;
      /** 设置多个open-area-left-side组件，数组每一项为LeftSideOption对象 */
      leftsideAreaList?: LeftSideOption[];
      /** 自定义参数，组件open-area-left、open-area-right、open-area-left-side均可通过属性获取到此参数 */
      ext?: string;
    }

    export interface SetVideoOptionOption {
      /** 同video组件的object-fit属性，默认会根据视频宽高进行计算 */
      objectFit?: string;
      /** 视频高宽比，当objectFit为fill时有效 */
      aspectRatio?: number;
      /** 是否循环播放，默认false */
      loop?: boolean;
    }
    export interface OnDataReportEvent {
      /** 播放器页面唯一ID，可通过此参数获取到PlayletManager实例 */
      playerId: string;
      /** 当前播放到第几集 */
      serialNo: number | string;
      /** 扩展参数，如手动decodeURIComponent()编码，过需要用decodeURIComponent()解码  */
      extParam: string;
      /** 当前播放到的时间 单位秒  */
      playTime: number;
      /** 当前剧集的总时长 单位秒   */
      duration: number;
      /** 当前剧集总集数  */
      totalCount: number;
      /** 小程序的appid  */
      appid?: number;
      /** 审核方的appid  */
      srcAppid?: number;
      /** 当前播放的的时间  */
      currentTime?: number;
      /** 剧目id */
      dramaId?: string;
      /** 剧目名称 */
      dramaname?: string;
      /** 事件名称 */
      event: `${REPORT_DATA_EVENTS}`;
    }

    export interface LoggerConfigOption {
      /** 是否输出 info 日志，默认为 false */
      info?: boolean;
      /** 是否输出 debug 日志，默认为 false */
      debug?: boolean;
      /** 是否输出 log 日志，默认为 false */
      log?: boolean;
      /** 是否输出 warn 日志，默认为 true */
      warn?: boolean;
      /** 是否输出 error 日志，默认为 true */
      error?: boolean;
    }

    export interface PlayletManagerInstance {
      /**
       * 设置可播放剧集
       */
      setCanPlaySerialList: (param: SetCanPlaySerialListParam) => void;

      /**
       * 在用户观看过程中重新设置免费剧列表，注意：此接口仅改变目录中的剧集显示状态，并不会做真正的解锁处理
       */
      setFreeList: (param: SetFreeListParam) => void;

      /**
       * 获取当前剧集信息
       */
      getInfo: () => ParamInfo;

      /**
       * 播放到需要解锁的剧集触发此事件，开发者可在此事件内解锁剧集
       */
      onCheckIsCanPlay: (event: OnCheckIsCanPlay) => void;

      /**
       * 设置可播放剧集
       */
      isCanPlay: (event: IsCanPlayEvent) => void;

      /**
       * 充值弹窗打开事件
       * @param event - 监听事件的回调方法
       */
      onShowChargeDialog: (event: () => void) => void;

      /**
       * 充值弹窗关闭事件监听
       * @param event - 监听事件的回调方法
       */
      onHideChargeDialog: (event: (op?:OnHideChargeDialogEvent) => void) => void;

      /**
       * 关闭充值弹窗
       */
      hideChargeDialog: () => void;
      /**
       * 打开充值弹窗
       */
      showChargeDialog: () => void;

      /**
       * 继续播放
       */
      play: () => void;
      /**
       * 暂停播放
       */
      pause: () => void;

      /**
       * 播放器内相关事件的回调函数
       */
      onDataReport: (event: (option: OnDataReportEvent) => void) => void;
      /**
       * 播放器内左上角返回点击事件
       */
      onBack: (event: () => void) => void;

      /**
       * 更换剧目
       */
      changeDrama: (option: ChangeDramaEvent) => void;
      /** 更新充值弹窗 */
      updateChargeDialogInfo: (option: UpdateChargeDialogInfoEvent) => void;
      /** 更新充值弹窗高度 */
      updateChargeDialogHeight: (option: UpdateChargeDialogHeightEvent) => void;
      /**
       * 设置截屏/录屏时屏幕表现,接口同wx.setVisualEffectOnCapture
       */
      setVisualEffectOnCapture: (option: UniNamespace.SetVisualEffectOnCaptureOption) => void;
      /** 设置分享参数 */
      setDramaFlag: (option: SetExtParamOption) => void;

      /**
       * 修改extParam参数
       * @param extParam - 修改后的参数，建议使用encodeURIComponent()编码
       */
      setExtParam: (extParam: string) => void;

      /**
       * 注册自定义事件，用于在充值组件、playletManager、自定义运营位等组件间通信
       * @param name - 事件名称
       * @param func - 触发事件后的回调
       */
      onCustomEvent: (name: string, func: (data: any) => Promise<void> | void) => void;

      /**
       * 销毁通过onCustomEvent注册的事件
       * @param name - 事件名称
       */
      offCustomEvent: (name: string, func?: (data: any) => Promise<void> | void) => void;

      /**
       * 销毁通过onCustomEvent注册的事件
       * @param name - 事件名称
       * @param args - 参数
       */
      emitCustomEvent: <T>(name: string, args: T) => void;

      /** 设置自定义开放区域参数 */
      updateOpenArea: (option: UpdateOpenAreaOption) => void;
      /** 设置播放器的播放配置 */
      setVideoOption: (option: SetVideoOptionOption) => void;
      /** 设置播放器的日志输出 */
      setLoggerConfig: (option: LoggerConfigOption) => void;

      /** 接口含义和参数同wx.navigateTo。 注意：接口调用成功的前提是用户必须至少点击过页面。 */
      navigateTo: <T extends UniNamespace.NavigateToOptions = UniNamespace.NavigateToOptions>(
        options: T
      ) => UniNamespace.PromisifySuccessResult<T, UniNamespace.NavigateToOptions>;

      /** 接口含义和参数同wx.redirectTo。 注意：接口调用成功的前提是用户必须至少点击过页面。 */
      redirectTo: <T extends UniNamespace.RedirectToOptions = UniNamespace.RedirectToOptions>(
        options: T
      ) => UniNamespace.PromisifySuccessResult<T, UniNamespace.RedirectToOptions>;

      /** 接口含义和参数同wx.reLaunch。 注意：接口调用成功的前提是用户必须至少点击过页面。 */
      reLaunch: <T extends UniNamespace.ReLaunchOptions = UniNamespace.ReLaunchOptions>(
        options: T
      ) => UniNamespace.PromisifySuccessResult<T, UniNamespace.ReLaunchOptions>;

      /** 接口含义和参数同wx.switchTab。 注意：接口调用成功的前提是用户必须至少点击过页面。 */
      switchTab: <T extends UniNamespace.SwitchTabOptions = UniNamespace.SwitchTabOptions>(
        options: T
      ) => UniNamespace.PromisifySuccessResult<T, UniNamespace.SwitchTabOptions>;

      /** 接口含义和参数同wx.navigateBack。 注意：接口调用成功的前提是用户必须至少点击过页面。 */
      navigateBack: <T extends UniNamespace.NavigateBackOptions = UniNamespace.NavigateBackOptions>(
        options?: T
      ) => UniNamespace.PromisifySuccessResult<T, UniNamespace.NavigateBackOptions>;
    }
    export interface PlayletManager {
      /**
       * 得到播放页面的实例
       * @param playerId - 播放器页面实例的唯一ID 由于可能页面栈上会存在多个播放器页面，所以需要通过这个唯一的ID来区分不同的页面，例如从播放器的推荐跳转到另外一个剧。
       */
      getPageManager: (playerId: PlayerId) => PlayletManagerInstance;
    }

    export interface OnPageLoadParam {
      [x: string]: unknown;
      /** 广告参数 */
      adParam: string;
      /** 短剧广告类型 */
      dramaAdType: number;
      /** 剧目ID，通过小程序媒资管理提审的剧目id */
      dramaId: string;
      /** 扩展参数，如手动decodeURIComponent()编码，过需要用decodeURIComponent()解码  */
      extParam: string;
      /** 播放器页面实例的唯一ID，由于可能页面栈上会存在多个播放器页面，所以需要通过这个唯一的ID来区分不同的页面，例如从播放器的推荐跳转到另外一个剧。 */
      playerId: string;
      /** 剧集ID，从1开始递增，比如第1集则serialNo是1，注意这里跟媒资管理的mediaNo是不一样的 */
      serialNo: 1;
      /**是提审方的appid */
      srcAppid: string;
      srcDramaId: string;
    }

    export interface OnPageLoad {
      (event: OnPageLoadParam): void;
    }

    export interface PlayletPlugin {
      /**
       * onPageLoad就是播放器页面的onLoad事件，接受的参数是函数，直接在app.js的onLaunch里面调用即可
       */
      onPageLoad: (event: OnPageLoad) => void;

      /**
       * 接口返回当前的插件版本，例如0.1.4
       */
      getPluginVersion: () => string;

      /**
       * 接口返回Promise实例
       * 当从App跳转、分享、公众号、二维码等场景，直接进入到播放器页面的时候，开发者可通过此接口获取跳转到页面链接上的参数。
       * 注意：如果不是直接进入播放器页面的，返回Promise.reject({err: '首个页面不是播放器页面'})。
       */
      getShareParams: () => Promise<Record<string, unknown>>;

      /**
       * PlayletManager是一个类，
       * 可通过playletPlugin.PlayletManager.getPageManager(playerId)获取其实例，
       * 大部分的接口都在该实例对象上提供，例如getInfo、showChargeDialog等
       */
      PlayletManager: PlayletManager;

      /** 上报事件 */
      REPORT_DATA_EVENTS: typeof REPORT_DATA_EVENTS;
    }
  }
}
