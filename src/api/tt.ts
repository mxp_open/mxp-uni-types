/* eslint-disable @typescript-eslint/no-namespace,no-use-before-define */
export {};
declare global {
  namespace tt {
    /** 拉起用户支付收银台成功 */
    export type GetOrderPaymentSuccess = {
      /** 抖音开放平台内部的交易订单号，len(orderId) <= 64byte，示例：motb37468756987 */
      orderId: string;
      /** 可供服务端排查问题 */
      logId?: (data: RequestOrderSuccess) => void;
    };

    /** 拉起用户支付收银台配置 */
    export type GetOrderPaymentOptions = {
      /** 抖音开放平台内部的交易订单号，len(orderId) <= 64byte，示例：motb37468756987 */
      orderId: string;
      /**
       * 接口调用成功的回调函数
       * 重要提示：success 回调仅代表 js_api 调用成功，不可用于判断用户实际支付成功，支付成功判断请开发者以服务端支付成功回调为准。
       */
      success?: (data: GetOrderPaymentSuccess) => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: RequestOrderFail) => void;
      /** 接口调用结束的回调函数（调用成功、失败都会执行） */
      complete?: (data: RequestOrderFail | RequestOrderSuccess) => void;
    };

    /** 下单成功 */
    export type ItemOrderList = {
      /** 交易系统商品单号，示例：motb874637654774 */
      itemOrderId: string;
      /** 商品id，开发者下单时传入的商品id */
      skuId: string;
      /** item单实付金额 */
      itemOrderAmount: number;
    };

    /** 下单成功 */
    export type RequestOrderSuccess = {
      /** 抖音开放平台内部的交易订单号，拉起收银台的参数，示例：motb123456789 */
      orderId: string;
      /** 订单信息 */
      itemOrderList: ItemOrderList[];
      /** 日志ID 可供服务端排查问题 */
      logId?: string;
    };

    /** 下单失败 */
    export type RequestOrderFail = {
      /** 错误码，对应信息可查看 errNo 说明 */
      errNo: string;
      /** 错误信息提示  */
      errMsg: string;
      /** 当下单失败时会提供该数据，可供服务端排查问题  */
      errLogId: string;
    };

    /** 通用交易下单配置 */
    export type RequestOrderOptions = {
      /** 下单信息 由开发者服务端返回 */
      data: string;
      /** 签名信息 由开发者服务端返回 */
      byteAuthorization: string;
      /** 接口调用成功的回调函数 */
      success?: (data: RequestOrderSuccess) => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: RequestOrderFail) => void;
      /** 接口调用结束的回调函数（调用成功、失败都会执行） */
      complete?: (data: RequestOrderFail | RequestOrderSuccess) => void;
    };

    /** 头条的订单信息 */
    export type OderInfo = {
      /** 担保交易服务端订单号 */
      order_id: string;
      /** 担保交易订单号 token */
      order_token: string;
    };
    /** 头条支付 AIP参数 */
    export type PayOptions = {
      /** 固定值：5（拉起小程序收银台） */
      service: 5;
      /** 调起小程序支付收银台的订单信息 */
      orderInfo: OderInfo;
      /** 仅限调试用，上线前去掉该参数。_debug=1 时，微信支付期间可以看到中间报错信息，方便调试 */
      _debug: 0 | 1;
      /** 接口调用成功的回调函数 */
      success?: () => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: unknown) => void;
    };
    export type PlayletShareParam = {
      /** 要转发的小程序标题 */
      title?: string;
      /** 这是默认的转发文案，用户可以直接发送，也可以在发布器内修改 */
      desc?: string;
      /** 示例：'page/xxx/index??tt_album_id=${ablumId}&xxx=xxx' 后面的参数会在转发页面打开时传入onLoad方法 */
      path?: string;
      /** 支持本地或远程图片，默认是小程序 icon */
      imageUrl?: string;
      /** 是开发者后台设置的分享素材模板id  */
      templateId?: string;
    };

    export type PlayletActivityInfo = {
      /** 自定义icon的图片(暂不支持本地图片) */
      icon: string;
      /** 自定义icon的标题，须填写1-3位汉字 */
      title: string;
    };

    export type BaseAlbumInfo = {
      /** 剧目ID */
      albumId: string;
      /** 剧集id */
      episodeId: string;
      /** 第几集 */
      seq: number;
    };

    export type PlayletInfo = BaseAlbumInfo & Record<string, any>;

    export type PlayletSetConfigOptions = {
      /** 配置右下角自定义button，当前仅展示Array中的第一个icon */
      activityInfo: PlayletActivityInfo[];
      /** 小程序分享参数 */
      shareParam: PlayletShareParam;
      /** 是否展示解锁页，默认为true */
      showLockPage: boolean;
      /** 设置视频展现形式，竖屏场景默认为cover，横屏场景默认为contain */
      objectFit: 'contain' | 'cover' | 'fill';
    };

    export type PlayletLockList = {
      /** 开始集数 (从1开始) */
      start_episode_no: number;
      /** 结束集数 */
      end_episode_no: number;
    };
    export type PlayletFreeList = {
      /** 开始集数 (从1开始) */
      start_episode_no: number;
      /** 结束集数 */
      end_episode_no: number;
    };
    export type PlayletUnlockList = {
      /** 开始集数 (从1开始) */
      start_episode_no: number;
      /** 结束集数 */
      end_episode_no: number;
    };
    export type PlayletSetCatalogOptions = {
      /** 配置右下角自定义button，当前仅展示Array中的第一个icon */
      lockList: PlayletLockList[];
      /** 小程序分享参数 */
      unlockList: PlayletUnlockList[];
      /** 是否展示解锁页，默认为true */
      freeList: PlayletFreeList[];
    };
    export type PlayletFeedRecommendData = {
      /** 推荐的剧目ID */
      albumId: string;
      /** 第几集 */
      seq: number;
    };

    export type PlayletRecommendConfig = {
      /** 推荐位类型 (槽位) */
      entryType?: 1 | 2 | 3;
      /** 是否开启 */
      switchStatus?: boolean;
      /** 推荐的剧目 */
      data?: PlayletFeedRecommendData | PlayletFeedRecommendData[];
    };

    export type PlayletOnPlayEvent = BaseAlbumInfo;
    export type PlayletOnPauseEvent = BaseAlbumInfo;
    export type PlayletOnEndedEvent = BaseAlbumInfo;
    export type PlayletOnWaiting = BaseAlbumInfo;
    export type PlayletOnTapShare = BaseAlbumInfo;
    export type PlayletOnTapCustomIcon = BaseAlbumInfo & {
      /** 点击的第几个 */
      index: number;
    };

    export type PlayletOnLoadedMetaData = BaseAlbumInfo & {
      /** 视频时长 */
      duration: number;
      /** 视频高度 */
      height: number;
      /** 视频宽度 */
      width: number;
    };

    export type PlayletOnTimeUpdate = BaseAlbumInfo & {
      /** 视频时长 */
      duration: number;
      /** 当前播放时间 */
      currentTime: number;
    };
    export type PlayletOnProgress = BaseAlbumInfo & {
      /** 百分比，取值是 [0, 100] 中的整数，如 buffered 为 50 表示当前视频缓冲了 50%。 */
      buffered: number;
    };
    export type PlayletOnPlayBackRateChange = BaseAlbumInfo & {
      /** 视频倍速 */
      playbackRate: number;
    };

    export type PlayletOnAddShortCut = BaseAlbumInfo & {
      /** 播放器展示、点击、关闭加桌按钮 */
      type: 'click' | 'close' | 'show';
      /** 加桌位置：底部、功能区 */
      position: 'bottom' | 'function_area';
    };

    export enum PlayletControlType {
      /** 播放控件 */
      play = 'play',
      /** 暂停控件 */
      pause = 'pause',
      /** 在倍速选择面板点击具体速度 */
      playbackRate = 'playbackRate',
      /** 进度控件，在刚开始拖动进度条时触发  */
      progress = 'progress',
      /** 点击喜欢 */
      like = 'like',
      /** 点击不喜欢 */
      unlike = 'unlike',
      /** 点击分享 */
      share = 'share',
      /** 点击追剧 */
      subscribe = 'subscribe',
      /** 点击取消追剧 */
      unsubscribe = 'unsubscribe',
      /** 点击打开目录页 */
      openCatalog = 'openCatalog',
      /** 点击关闭目录页 */
      closeCatalog = 'closeCatalog',
      /** 用户点击蒙层关闭充值页 */
      userCloseCharge = 'userCloseCharge',
    }

    export type PlayletOnControlTap = BaseAlbumInfo & {
      [x: string]: unknown;
      /** 返回当前点击的控件类型 */
      controlType: `${PlayletControlType}`;
    };

    export type PlayletOnOpenCatalog = BaseAlbumInfo & {
      /** 0表示关闭,1表示打开 */
      status: 0 | 1;
    };
    export type PlayletOnChangeEpisode = BaseAlbumInfo & {
      /** free免费, lock锁定 ,unlock解锁 */
      status: 'free' | 'lock' | 'unlock';
    };
    export type PlayletOnClickUnlock = BaseAlbumInfo;

    export type PlayletManager = {
      /** 开发者获取当前剧目信息 (在ready 生命周期的时候，页面逻辑刚开始执行，一般剧集信息还拿不到, 可以考虑在切换剧集的时候拿) */
      getPlayletInfo: () => Promise<PlayletInfo>;
      /** 开发者设置分享信息和自定义icon */
      setConfig: (data: PlayletSetConfigOptions) => void;
      /** 开发者主动更新当前目录 */
      setCatalog: (data: PlayletSetCatalogOptions) => void;
      /** 开发者（打开 /关闭）自定义充值模块 */
      toggleCustomDialog: (type?: 'close' | 'open') => void;
      /** 开发者设置当前集为解锁 */
      setCurrentUnlock: () => void;
      /** 开发者设置暂停/播放状态 */
      setPlayStatus: (status?: 'pause' | 'play') => void;
      /** 设置推荐位 */
      setRecommendConfig: (data?: PlayletRecommendConfig) => void;
      /** 当开始播放时触发 play 事件 */
      onPlay: (event: (option: PlayletOnPlayEvent) => void) => void;
      /** 当暂停播放时触发 pause 事件。  */
      onPause: (event: (option: PlayletOnPauseEvent) => void) => void;
      /** 当播放到末尾时触发 ended 事件 */
      onEnded: (event: (option: PlayletOnEndedEvent) => void) => void;
      /** 视频播放出错时触发 error 事件 */
      onError: (event: (option: any) => void) => void;
      /** 播放进度变化时触发，返回当前播放时间点及视频总时长，单位：秒(s) */
      onTimeUpdate: (event: (option: PlayletOnTimeUpdate) => void) => void;
      /** 视频缓冲进度更新时触发 */
      onProgress: (event: (option: PlayletOnProgress) => void) => void;
      /** 视频出现缓冲时触发 */
      onWaiting: (event: (option: PlayletOnWaiting) => void) => void;
      /** 视频倍速改变完成时触发。返回改变后的倍速值。 */
      onPlayBackRateChange: (event: (option: PlayletOnPlayBackRateChange) => void) => void;
      /** 视频元数据加载完成时触发 */
      onLoadedMetaData: (event: (option: PlayletOnLoadedMetaData) => void) => void;
      /** 点击控件时触发。返回当前点击的控件类型。取值见表 controlType 的合法值。 注意 : IDE上的（点赞、点亮）是模拟的，不是真实抖音用户 */
      onControlTap: (event: (option: PlayletOnControlTap) => void) => void;
      /** 用户点击选集或关闭选集，通知开发者该事件 */
      onOpenCatalog: (event: (option: PlayletOnOpenCatalog) => void) => void;
      /** 用户点击选集页中的某一集，或上划下划切换集，通知开发者该事件 */
      onChangeEpisode: (event: (option: PlayletOnChangeEpisode) => void) => void;
      /** 观众点击了“立即解锁”按钮 */
      onClickUnlock: (event: (option: PlayletOnClickUnlock) => void) => void;
      /** 观众点击了“分享”按钮 */
      onTapShare: (event: (option: PlayletOnTapShare) => void) => void;
      /** 分享失败 */
      onShareFail: (event: (option: any) => void) => void;
      /** 分享成功 */
      onShareSuccess: (event: (option: any) => void) => void;
      /**点击右下角自定义icon */
      onTapCustomIcon: (event: (option: PlayletOnTapCustomIcon) => void) => void;
      /** 加桌相关能力发生展示、关闭、点击时触发  */
      onAddShortCut: (event: (option: PlayletOnAddShortCut) => void) => void;
    };

    export type TTMicroApp = {
      /** 小程序版本号 */
      mpVersion: string;
      /** 小程序环境 development 测试版、preview 预览版、production 线上版 */
      envType: string;
      /** 小程序 appId */
      appId: string;
    };
    export type TTCommon = {
      /** 用户数据存储的路径（默认值 ttfile://user） */
      USER_DATA_PATH: string;
    };

    export type TTEnvInfoSync = {
      /** 小程序信息 */
      microapp: TTMicroApp;
      /** 通用参数 */
      common: TTCommon;
    };

    export type RewardedVideoAdOptions = {
      /** 广告类型 */
      type: number;
      /** 广告id */
      unitId: number;
    };

    /** 短视频播放器扩展 */
    function PlayletExtension(data?: any): void;
    /** 得到短视频播放器扩展实例 */
    function getPlayletManager(data?: any): PlayletManager;
    /** 担保支付支付 */
    export function pay(data: PayOptions): void;
    /** 提供预下单能力，开发者通过调用该方法生成订单，返回订单号和订单信息 */
    export function requestOrder(data: RequestOrderOptions): void;
    /** 拉起用户支付收银台 */
    export function getOrderPayment(data: GetOrderPaymentOptions): void;
    /** 拉起用户支付收银台 */
    export function getEnvInfoSync(): TTEnvInfoSync;
    /** 创建激励视频广告 */
    function createRewardedVideoAd(data: RewardedVideoAdOptions): UniApp.RewardedVideoAdContext;
  }
}
