/* eslint-disable @typescript-eslint/no-namespace */
export {};
declare global {
  namespace ks {
    /** 快手的订单信息 */
    export type OderInfo = {
      /** 订单号 */
      order_no: string;
      /** 订单 token */
      order_info_token: string;
    };

    /** 支付渠道信息 */
    export type PaymentChannel = {
      /** 通道类型 */
      provider: string;
      /** 金融渠道的通道类型 */
      provider_channel_type: string;
    };

    /** 头条支付 AIP参数 */
    export type PayOptions = {
      /** 服务类型 id（固定值为 '1'） */
      serviceId: '1';
      /** 订单数据 */
      orderInfo: OderInfo;
      /** 支付渠道信息（来自 <payment-list> 组件） */
      paymentChannel?: PaymentChannel;
      /** 支付类型 （固定值'IAPPurchase',仅作用于IAP支付） */
      payType?: 'IAPPurchase';

      /** 接口调用成功的回调函数 */
      success?: () => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: Error) => void;
      /** 接口调用结束的回调函数（调用成功、失败都会执行） */
      complete?: (error: Error) => void;
    };

    export type RewardedVideoAdOptions = {
      /** 广告类型 */
      type: number;
      /** 广告id */
      unitId: number;
    };

    /** 支付 */
    function pay(data: PayOptions): void;

    /** 创建激励视频广告 */
    function createRewardedVideoAd(data: RewardedVideoAdOptions): UniApp.RewardedVideoAdContext;
  }
}
