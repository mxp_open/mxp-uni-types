/* eslint-disable @typescript-eslint/no-namespace */
export {};
declare global {
  namespace wx {
    export type RequestVirtualPaymentSuccess = {
      /** 调用成功信息 */
      errMsg: string;
    };
    export type RequestVirtualPaymentFail = {
      /** 调用失败信息 */
      errMsg: string;
      /** 错误码 */
      errCode: number;
    };
    export type RequestPaymentFail = {
      /** 调用失败信息 */
      errMsg: string;
      /** 错误码 */
      errCode?: number;
    };
    export type RequestPaymentSuccess = {
      /** 调用成功信息 */
      errMsg: string;
    };
    export type RequestVirtualOptions = {
      /** 时间戳，从 1970 年 1 月 1 日 00:00:00 至今的秒数，即当前的时间 */
      timeStamp: string;
      /** 随机字符串，长度为32个字符以下 */
      nonceStr: string;
      /** 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=*/
      package: string;
      /** 签名算法，应与后台下单时的值一致 */
      signType?: 'HMAC-SHA256' | 'MD5' | 'RSA';
      /** 签名，具体见微信支付文档 */
      paySign: string;
      /**
       * 接口调用成功的回调函数
       * 重要提示：success 回调仅代表 js_api 调用成功，不可用于判断用户实际支付成功，支付成功判断请开发者以服务端支付成功回调为准。
       */
      success?: (data: RequestPaymentSuccess) => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: RequestVirtualPaymentFail) => void;
      /** 接口调用结束的回调函数（调用成功、失败都会执行） */
      complete?: (data: RequestVirtualPaymentFail | RequestVirtualPaymentSuccess) => void;
    };
    export type RequestVirtualPaymentOptions = {
      /**
       * 具体支付参数见signData, 该参数需以string形式传递,
       * 例如signData: '{"offerId":"123","buyQuantity":1,"env":0,"currencyType":"CNY","productId":"testproductId","goodsPrice":10,"outTradeNo":"xxxxxx","attach":"testdata"}'
       */
      signData: string;
      /**
       * 支付的类型, 不同的支付类型有各自额外要传的附加参数
       * 合法值	说明
       * short_series_goods	道具直购
       * short_series_coin	代币充值
       */
      mode: 'short_series_coin' | 'short_series_goods';
      /** 支付签名, 详见《签名详解》 */
      paySig: string;
      /** 用户态签名, 详见《签名详解》 */
      signature: string;

      /**
       * 接口调用成功的回调函数
       * 重要提示：success 回调仅代表 js_api 调用成功，不可用于判断用户实际支付成功，支付成功判断请开发者以服务端支付成功回调为准。
       */
      success?: (data: RequestVirtualPaymentSuccess) => void;
      /** 接口调用失败的回调函数 */
      fail?: (error: RequestPaymentFail) => void;
      /** 接口调用结束的回调函数（调用成功、失败都会执行） */
      complete?: (data: RequestPaymentFail | RequestVirtualPaymentSuccess) => void;
    };

    /** 微信支付 */
    function requestPayment(data: RequestVirtualOptions): void;

    /** 微信虚拟支付 */
    function requestVirtualPayment(data: RequestVirtualPaymentOptions): void;
    /** 环境变量 */
    export let env: {
      [key: string]: string;
      /** 文件系统中的用户目录路径 (本地路径) */
      USER_DATA_PATH: string;
    };
  }
}
